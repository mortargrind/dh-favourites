const path = require("path");

// Jest configuration
// https://facebook.github.io/jest/docs/en/configuration.html
module.exports = {
  automock: false,

  browser: false,

  bail: true,

  verbose: true,

  collectCoverageFrom: [
    "src/**/*.{js,jsx}",
    "!**/node_modules/**",
  ],

  coverageReporters: ["text"],

  coverageDirectory: "<rootDir>/coverage",

  globals: {
    window: true,
  },

  moduleFileExtensions: ["js", "json", "jsx", "mjs"],

  moduleNameMapper: {
    "^.+\\.(css|scss)$": "<rootDir>/src/test/mock/styleMock.js",
    "^.+\\.(gif|ttf|eot|png)$": "<rootDir>/src/test/mock/fileMock.js",
    "^common(.*)$": "<rootDir>/src/common/$1",
    "^component(.*)$": "<rootDir>/src/component/$1",
  },

  setupFiles: ["<rootDir>/src/test/setupTests.js"],

  testPathIgnorePatterns: [
    "/node_modules/"
  ]
};
