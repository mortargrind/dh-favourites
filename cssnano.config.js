const defaultPreset = require("cssnano-preset-default");

module.exports = defaultPreset({
  mergeLonghand: false,
});
