module.exports = function (api) {
  const env = api.env();
  const isRelease = process.argv.includes("--release");

  const basePlugins = [
    "@babel/plugin-proposal-class-properties",
    "@babel/plugin-transform-runtime",
  ];

  const basePresets = [];

  const presets = [...basePresets];
  const plugins = [...basePlugins];

  if (env === "test") {
    presets.push([
      "@babel/preset-env",
      {
        useBuiltIns: "entry",
        corejs: 3,
      },
    ]);
  } else {
    if (isRelease) {
      plugins.push([
        "transform-react-remove-prop-types",
        {
          mode: "remove",
          removeImport: true,
          additionalLibraries: [],
        },
      ]);
    } else {

      plugins.push(
        "react-hot-loader/babel"
      );
    }

    presets.push(
      [
        "@babel/preset-env",
        {
          useBuiltIns: "entry",
          modules: false,
          corejs: 3,
        },
      ],
    );
  }

  presets.push(
    "@babel/preset-react",
  );

  return {
    presets,
    plugins,
  };
};
