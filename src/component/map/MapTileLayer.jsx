import React, {Component} from "react";
import {TileLayer} from "react-leaflet";

const TILE_URL = `//api.mapbox.com/styles/v1/mapbox/light-v9/tiles/256/{z}/{x}/{y}?access_token=${process.env.DH_MAPBOX_ACCESS_TOKEN}`;

class MapTileLayer extends Component {
  render() {
    return (
      <TileLayer
        attribution={"&amp;<a href='https://www.mapbox.com/about/maps/'>Mapbox</a> © <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a> <strong><a href='https://www.mapbox.com/map-feedback/' target='_blank'>Improve this map</a></strong>"}
        url={TILE_URL}
        {...this.props}/>
    );
  }
}

export default MapTileLayer;
