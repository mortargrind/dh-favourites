import React, {PureComponent} from "react";
import PropTypes from "prop-types";

class TypeAheadResultListItem extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    result: PropTypes.object.isRequired,
    onSelect: PropTypes.func.isRequired,
  }

  handleSelect = () => {
    const {
      onSelect,
      result,
    } = this.props;

    onSelect(result);
  }

  render() {
    const {
      className,
      children,
    } = this.props;

    return (
      <li
        className={className}
        onClick={this.handleSelect}>
        {children}
      </li>
    );
  }
}

export default TypeAheadResultListItem;
