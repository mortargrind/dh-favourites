import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import TypeAheadResultListItem from "./result/TypeAheadResultListItem";
import CrescentSpinner from "component/spinner/crescent/CrescentSpinner";

import styles from "./TypeAhead.scss";

class TypeAhead extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    resultListContainerClassName: PropTypes.string,
    resultListClassName: PropTypes.string,
    resultListItemClassName: PropTypes.string,
    inputClassName: PropTypes.string,
    inputContainerClassName: PropTypes.string,
    resultSpinnerClassName: PropTypes.string,
    inputValue: PropTypes.string,
    inputPlaceholder: PropTypes.string,
    results: PropTypes.array,
    isDisabled: PropTypes.bool,
    isPending: PropTypes.bool,
    renderResult: PropTypes.func,
    renderInput: PropTypes.func,
    resultIdGenerator: PropTypes.func.isRequired,
    onInputChange: PropTypes.func,
    onInputFocus: PropTypes.func,
    onInputBlur: PropTypes.func,
    onResultSelect: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      isChildrenClicked: false,
      isFocused: false,
    };
  }

  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  handleClickOutside = (event) => {
    if (this.rootRef && !this.rootRef.contains(event.target)) {
      const {isFocused} = this.state;

      this.setState({
        isChildrenClicked: false
      });

      if (isFocused) {
        this.handleDeactivation();
      }
    }
  };

  handleDeactivation = () => {
    this.setState({
      isFocused: false,
    });
  };

  handleMouseDown = () => {
    this.setState({
      isChildrenClicked: true
    });
  };

  handleResultSelect = (result) => {
    const {onResultSelect} = this.props;

    this.handleDeactivation();

    onResultSelect(result);
  };

  handleInputChange = (event) => {
    const {onInputChange} = this.props;

    onInputChange(event);
  };

  handleInputFocus = (event) => {
    const {onInputFocus} = this.props;

    this.setState({
      isFocused: true,
    });

    if (onInputFocus) {
      onInputFocus(event);
    }
  };

  handleInputBlur = () => {
    const {onInputBlur} = this.props;
    const {
      isChildrenClicked,
      isFocused
    } = this.state;

    if (isFocused && !isChildrenClicked) {
      this.handleDeactivation();
    }

    if (onInputBlur) {
      onInputBlur(event);
    }
  };

  refCallback = (element) => {
    this.rootRef = element;
  };

  renderInput() {
    const {
      isDisabled,
      inputPlaceholder,
      inputClassName,
      inputValue,
      renderInput,
    } = this.props;

    let input;

    if (renderInput) {
      input = renderInput({
        className: cn(styles.input, inputClassName),
        placeholder: inputPlaceholder,
        value: inputValue,
        disabled: isDisabled,
        onChange: this.handleInputChange,
        onBlur: this.handleInputBlur,
        onFocus: this.handleInputFocus,
      });
    } else {
      input = (
        <input
          className={cn(styles.input, inputClassName)}
          placeholder={inputPlaceholder}
          value={inputValue}
          disabled={isDisabled}
          onChange={this.handleInputChange}
          onBlur={this.handleInputBlur}
          onFocus={this.handleInputFocus}/>
      );
    }
    return input;
  }

  renderResults() {
    const {
      results,
      renderResult,
      resultListClassName,
      resultListItemClassName,
      resultIdGenerator,
    } = this.props;

    return (
      <ul className={cn(styles.resultList, resultListClassName)}>
        {results.map(result => (
          <TypeAheadResultListItem
            key={resultIdGenerator(result)}
            result={result}
            className={cn(styles.resultListItem, resultListItemClassName)}
            onSelect={this.handleResultSelect}>
            {renderResult(result)}
          </TypeAheadResultListItem>
        ))}
      </ul>
    );
  }

  render() {
    const {
      className,
      inputContainerClassName,
      resultListContainerClassName,
      resultSpinnerClassName,
      results,
      isPending,
    } = this.props;
    const {isFocused} = this.state;
    const rootClassName = cn(styles.root, className);

    let shouldRenderResults;

    if (results && results.length > 0 && isFocused) {
      shouldRenderResults = true;
    }

    return (
      <div
        ref={this.refCallback}
        className={rootClassName}
        onMouseDown={this.handleMouseDown}>

        <div className={cn(styles.inputContainer, inputContainerClassName)}>
          {this.renderInput()}
        </div>

        <div className={cn(styles.resultListContainer, resultListContainerClassName)}>
          {
            shouldRenderResults &&
            this.renderResults()
          }

          {
            isPending &&
            <div className={styles.resultSpinnerContainer}>
              <CrescentSpinner
                className={cn(styles.resultSpinner, resultSpinnerClassName)}/>
            </div>
          }
        </div>
      </div>
    );
  }
}

export default TypeAhead;
