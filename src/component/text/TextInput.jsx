import React, {Component} from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import styles from "./TextInput.scss";

class TextInput extends Component {
  static propTypes = {
    className: PropTypes.string,
    errorClassName: PropTypes.string,
    hasError: PropTypes.bool,
    type: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([
      PropTypes.number.isRequired,
      PropTypes.string.isRequired
    ]),
    name: PropTypes.string,
    maxLength: PropTypes.number,
    min: PropTypes.number,
    max: PropTypes.number,
    step: PropTypes.number,
    placeholder: PropTypes.string,
    isDisabled: PropTypes.bool,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    onFocus: PropTypes.func,
    onKeyPress: PropTypes.func,
    onKeyDown: PropTypes.func,
    autoFocus: PropTypes.bool
  };

  static defaultProps = {
    type: "text",
  };

  render() {
    const {
      className,
      errorClassName,
      name,
      value,
      type,
      placeholder,
      maxLength,
      max,
      min,
      step,
      onChange,
      onBlur,
      onFocus,
      onKeyPress,
      onKeyDown,
      isDisabled,
      autoFocus,
      hasError,
    } = this.props;
    const rootClassName = cn(
      "text-input",
      styles.root,
      className, {
        "has-error": hasError,
        [errorClassName]: errorClassName && hasError,
      }
    );

    return (
      <input
        className={rootClassName}
        name={name}
        type={type}
        value={value}
        placeholder={placeholder}
        maxLength={maxLength}
        min={min}
        max={max}
        step={step}
        autoFocus={autoFocus}
        disabled={isDisabled}
        onChange={onChange}
        onBlur={onBlur}
        onFocus={onFocus}
        onKeyPress={onKeyPress}
        onKeyDown={onKeyDown}/>
    );
  }
}

export default TextInput;
