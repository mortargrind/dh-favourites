import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import styles from "component/spinner/crescent/CrescentSpinner.scss";

class CrescentSpinner extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
  }

  render() {
    const {className} = this.props;
    const rootClassName = cx(styles.root, className);

    return (
      <div className={rootClassName}>
        <div className={styles.crescent}/>
        <div className={styles.crescent}/>
        <div className={styles.crescent}/>
      </div>
    );
  }
}

export default CrescentSpinner;
