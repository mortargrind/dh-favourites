import React from "react";
import {shallow} from "enzyme";
import renderer from "react-test-renderer";

import Button from "./Button";

it("renders one button element in root", () => {
  const wrapper = shallow(<Button/>);

  expect(wrapper.get(0).type).toBe("button");
});

it("adds given custom class name to the root", () => {
  const wrapper = shallow(<Button className={"my-button"}/>);

  expect(wrapper.first("button").hasClass("my-button")).toBe(true);
});

it("disables the button element when isDisabled is true", () => {
  const wrapper = shallow(<Button isDisabled={true}/>);

  expect(wrapper.first("button").prop("disabled")).toBe(true);
});

it("renders 'pending indicator' element when isPending is true", () => {
  const wrapper = shallow(<Button isPending={true}/>);

  expect(wrapper.find(".button__pending-indicator")).toHaveLength(1);
});

it("does not render 'pending indicator' element when isPending is false", () => {
  const wrapper = shallow(<Button isPending={false}/>);

  expect(wrapper.find(".button__pending-indicator")).toHaveLength(0);
});

it("renders text in '.button__text'", () => {
  const wrapper = shallow(<Button text={"My Text"}/>);

  expect(wrapper.find(".button__text")).toHaveLength(1);
  expect(wrapper.first(".button__text").text()).toBe("My Text");
});

it("does not render text when isPending is true", () => {
  const wrapper = shallow(
    <Button
      isPending={true}
      text={"My Text"}/>
  );

  expect(wrapper.find(".button__text")).toHaveLength(0);
});

it("does not render icon when isPending is true", () => {
  const wrapper = shallow(
    <Button
      isPending={true}
      hasIcon={true}/>
  );

  expect(wrapper.find(".button__icon")).toHaveLength(0);
});

it("renders 'icon-container' and 'icon' when hasIcon is true", () => {
  const wrapper = shallow(<Button hasIcon={true}/>);

  expect(wrapper.find(".button__icon-container")).toHaveLength(1);
  expect(wrapper.find(".button__icon")).toHaveLength(1);
});

it("correctly sets the type of the button when type is given", () => {
  const wrapper = shallow(<Button type={"submit"}/>);

  expect(wrapper.find("button").prop("type")).toBe("submit");
});

it("does not trigger click events when isPending is true", () => {
  const mockClickCallback = jest.fn();

  const wrapper = shallow(
    <Button
      isPending={true}
      onClick={mockClickCallback}/>);

  wrapper.simulate("click");
  expect(mockClickCallback.mock.calls.length).toBe(0);
});

// Snapshots
it("renders correctly", () => {
  const tree = renderer
    .create(<Button/>)
    .toJSON();

  expect(tree).toMatchSnapshot();
});

it("renders correctly with 'icon-container' and 'icon' when hasIcon is true", () => {
  const tree = renderer
    .create(<Button hasIcon={true}/>)
    .toJSON();

  expect(tree).toMatchSnapshot();
});

it("renders correctly without icon when 'hasIcon' is true but isPending is also true", () => {
  const tree = renderer
    .create(
      <Button
        hasIcon={true}
        isPending={true}/>
    ).toJSON();

  expect(tree).toMatchSnapshot();
});

it("renders with 'button__text' when text is not empty", () => {
  const tree = renderer
    .create(<Button text={"test text"}/>)
    .toJSON();

  expect(tree).toMatchSnapshot();
});

it("renders correctly without text when text is given but isPending is true", () => {
  const tree = renderer
    .create(
      <Button
        text={"test text"}
        isPending={true}/>
    )
    .toJSON();

  expect(tree).toMatchSnapshot();
});

it("renders as a non-pending button", () => {
  const tree = renderer
    .create(
      <Button
        text={"test text"}
        hasIcon={true}
        isPending={false}/>
    ).toJSON();

  expect(tree).toMatchSnapshot();
});

it("renders as a pending button", () => {
  const tree = renderer
    .create(
      <Button
        text={"test text"}
        hasIcon={true}
        isPending={true}/>
    ).toJSON();

  expect(tree).toMatchSnapshot();
});

it("button with icon and text", () => {
  const tree = renderer
    .create(
      <Button
        text={"test text"}
        hasIcon={true}
        isPending={false}/>
    ).toJSON();

  expect(tree).toMatchSnapshot();
});
