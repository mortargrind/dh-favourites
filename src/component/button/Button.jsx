import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import CrescentSpinner from "component/spinner/crescent/CrescentSpinner";

import styles from "./Button.scss";

class Button extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    contentClassName: PropTypes.string,
    textClassName: PropTypes.string,
    iconContainerClassName: PropTypes.string,
    iconClassName: PropTypes.string,
    pendingIndicatorClassName: PropTypes.string,
    tabIndex: PropTypes.number,
    text: PropTypes.string,
    hasIcon: PropTypes.bool,
    type: PropTypes.string,
    isDisabled: PropTypes.bool,
    isPending: PropTypes.bool,
    onClick: PropTypes.func,
    onMouseDown: PropTypes.func,
  };

  static defaultProps = {
    type: "button",
    hasIcon: false,
    isPending: false,
    tabIndex: 0,
  };

  handleMouseDown = (event) => {
    const {isPending, onMouseDown} = this.props;

    if (!isPending) {
      if (onMouseDown) {
        onMouseDown(event);
      }
    }
  }

  handleClick = (event) => {
    const {
      isPending,
      onClick,
      isDisabled,
    } = this.props;

    if (!isPending && !isDisabled) {
      if (onClick) {
        onClick(event);
      }
    }
  }

  render() {
    const {
      className,
      contentClassName,
      textClassName,
      iconClassName,
      iconContainerClassName,
      pendingIndicatorClassName,
      text,
      hasIcon,
      tabIndex,
      isDisabled,
      type,
      isPending,
    } = this.props;
    const containerClassName = cn(
      styles.root,
      "button",
      className,
      {
        [styles.isPending]: isPending,
      }
    );

    return (
      <button
        className={containerClassName}
        disabled={isDisabled}
        onClick={this.handleClick}
        onMouseDown={this.handleMouseDown}
        tabIndex={tabIndex}
        type={type}>

        <span className={cn(styles.content, contentClassName, "button__content")}>
          {
            !isPending && hasIcon &&
            <span className={cn(styles.iconContainer, iconContainerClassName, "button__icon-container")}>
              <span className={cn(styles.icon, iconClassName, "button__icon")}/>
            </span>
          }

          {
            !isPending &&
            text &&
            <span className={cn(styles.text, textClassName, "button__text")}>
              {text}
            </span>
          }

          {
            isPending &&
            <CrescentSpinner
              className={cn(styles.pendingIndicator, pendingIndicatorClassName, "button__pending-indicator")}/>
          }
        </span>
      </button>
    );
  }
}

export default Button;
