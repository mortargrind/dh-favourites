import React, {
  PureComponent,
  Fragment
} from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import CrescentSpinner from "component/spinner/crescent/CrescentSpinner";

import styles from "component/list/view/ListView.scss";

const propTypes = {
  className: PropTypes.string,
  listClassName: PropTypes.string,
  listItemClassName: PropTypes.string,
  emptyStateContainerClassName: PropTypes.string,
  items: PropTypes.array,
  isPending: PropTypes.bool.isRequired,
  emptyStateContent: PropTypes.node,
  canDisplayEmptyState: PropTypes.bool.isRequired,
  forceToEmptyStateWhilePending: PropTypes.bool,
  itemKeyGenerator: PropTypes.func,
  renderItem: PropTypes.func.isRequired,
};

const defaultProps = {
  isPending: false,
  canDisplayEmptyState: true,
  forceToEmptyStateWhilePending: false
};

class ListView extends PureComponent {
  static propTypes = propTypes;

  static defaultProps = defaultProps;

  renderListItem = (item, index) => {
    const {
      renderItem,
      itemKeyGenerator,
      listItemClassName,
    } = this.props;

    return (
      <li
        key={itemKeyGenerator(item)}
        className={cn(styles.listItem, listItemClassName)}>
        {renderItem(item, index)}
      </li>
    );
  };

  render() {
    const {
      className,
      listClassName,
      emptyStateContainerClassName,
      items,
      isPending,
      emptyStateContent,
      canDisplayEmptyState,
      forceToEmptyStateWhilePending
    } = this.props;
    const containerClassName = cn(
      styles.root,
      className
    );

    return (
      <div className={containerClassName}>
        {items && items.length > 0 && !(forceToEmptyStateWhilePending && isPending) ?
          <ul className={cn(listClassName)}>
            {items.map(item => this.renderListItem(item))}
          </ul> :
          <Fragment>
            {!isPending && canDisplayEmptyState && emptyStateContent &&
            <div className={cn(styles.emptyStateContainer, emptyStateContainerClassName)}>
              {emptyStateContent}
            </div>}
          </Fragment>
        }

        {isPending &&
        <CrescentSpinner className={styles.spinner}/>}
      </div>
    );
  }
}

export default ListView;
export {propTypes, defaultProps};
