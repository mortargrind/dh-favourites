import {
  takeEvery,
  select,
} from "redux-saga/effects";

import {
  actionNames as restaurantActionNames,
} from "./restaurantActions";
import {
  FAVOURITE_RESTAURANTS_STORAGE_KEY,
} from "./restaurantConstants";
import {
  favouriteRestaurantArraySelector,
} from "./restaurantSelectors";

function restaurantSagasFactory() {
  function* addFavouriteRestaurantSaga() {
    const favouriteRestaurantArray = yield select(favouriteRestaurantArraySelector);

    window.localStorage.setItem(
      FAVOURITE_RESTAURANTS_STORAGE_KEY,
      JSON.stringify(favouriteRestaurantArray)
    );
  }

  function* deleteFavouriteRestaurantSaga() {
    const favouriteRestaurantArray = yield select(favouriteRestaurantArraySelector);

    window.localStorage.setItem(
      FAVOURITE_RESTAURANTS_STORAGE_KEY,
      JSON.stringify(favouriteRestaurantArray)
    );
  }

  function* watchAddFavouriteRestaurant() {
    yield takeEvery(
      restaurantActionNames.ADD_FAVOURITE_RESTAURANT,
      addFavouriteRestaurantSaga,
    );
  }

  function* watchDeleteFavouriteRestaurant() {
    yield takeEvery(
      restaurantActionNames.DELETE_FAVOURITE_RESTAURANT,
      deleteFavouriteRestaurantSaga,
    );
  }

  const restaurantSagas = [
    watchAddFavouriteRestaurant(),
    watchDeleteFavouriteRestaurant(),
  ];

  return restaurantSagas;
}

export {
  restaurantSagasFactory,
};
