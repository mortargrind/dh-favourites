import {createSelector} from "reselect";

import {
  sortFavouriteRestaurantArrayByName,
  searchFavouriteRestaurantsByName,
} from "restaurant/restaurantUtils";

const favouriteRestaurantArraySelector = state => state.restaurantState.favouriteRestaurantArray;
const favouriteRestaurantOrderTypeSelector = state => state.restaurantState.favouriteRestaurantOrderType;
const favouriteRestaurantSearchTermSelector = state => state.restaurantState.favouriteRestaurantSearchTerm;

const displayedFavouriteRestaurantArraySelector = createSelector(
  [
    favouriteRestaurantArraySelector,
    favouriteRestaurantOrderTypeSelector,
    favouriteRestaurantSearchTermSelector,
  ],
  (
    favouriteRestaurantArray,
    favouriteRestaurantOrderType,
    favouriteRestaurantSearchTerm,
  ) => sortFavouriteRestaurantArrayByName(
    searchFavouriteRestaurantsByName(
      favouriteRestaurantArray,
      favouriteRestaurantSearchTerm,
    ),
    favouriteRestaurantOrderType,
  )
);

export {
  favouriteRestaurantArraySelector,
  displayedFavouriteRestaurantArraySelector,
};
