import {
  FAVOURITE_RESTAURANTS_STORAGE_KEY,
  DEFAULT_FAVOURITE_RESTAURANTS_VALUE,
  FAVOURITE_RESTAURANT_ORDER_TYPES,
} from "./restaurantConstants";

function generateRestaurantFromFormValues(values) {
  const creationDate = Date.now();

  return {
    id: `local_restaurant_id_${creationDate}`,
    name: values.name,
    rating: Number(values.rating),
    favouriteFood: values.favouriteFood,
    location: values.location,
    creationDate,
  };
}

function getFavouriteRestaurantsFromStorage() {
  let result;

  try {
    result = JSON.parse(
      window.localStorage.getItem(FAVOURITE_RESTAURANTS_STORAGE_KEY)
    );
  } catch (error) {
    console.error(error);
  }

  return result || DEFAULT_FAVOURITE_RESTAURANTS_VALUE;
}

function restaurantNameAscendingComparator(firstRestaurant, secondRestaurant) {
  const firstName = firstRestaurant.name.toLowerCase();
  const secondName = secondRestaurant.name.toLowerCase();

  return firstName.localeCompare(secondName);
}

function restaurantNameDescendingComparator(firstRestaurant, secondRestaurant) {
  const firstName = firstRestaurant.name.toLowerCase();
  const secondName = secondRestaurant.name.toLowerCase();

  return secondName.localeCompare(firstName);
}

function sortFavouriteRestaurantArrayByName(
  restaurantArray,
  orderType
) {
  let comparator;

  if (orderType === FAVOURITE_RESTAURANT_ORDER_TYPES.ALPHABETICAL_ASCENDING) {
    comparator = restaurantNameAscendingComparator;
  } else {
    comparator = restaurantNameDescendingComparator;
  }

  return [...restaurantArray].sort(comparator);
}

function searchFavouriteRestaurantsByName(
  restaurantArray,
  searchTerm,
) {
  const lowerCasedSearchTerm = searchTerm.toLowerCase();

  let resultArray;

  if (searchTerm) {
    resultArray = restaurantArray.filter(restaurant =>
      (restaurant.name.toLowerCase().indexOf(lowerCasedSearchTerm) > -1)
    );
  } else {
    resultArray = [...restaurantArray];
  }

  return resultArray;
}

export {
  generateRestaurantFromFormValues,
  getFavouriteRestaurantsFromStorage,
  sortFavouriteRestaurantArrayByName,
  searchFavouriteRestaurantsByName,
};
