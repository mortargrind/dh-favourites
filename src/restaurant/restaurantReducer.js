import {
  actionNames as restaurantActionNames,
} from "./restaurantActions";
import {
  getFavouriteRestaurantsFromStorage,
} from "./restaurantUtils";
import {
  findAndRemoveFromImmutableArray,
} from "common/collection/collectionUtils";
import {
  FAVOURITE_RESTAURANT_ORDER_TYPES,
} from "./restaurantConstants";

function generateDefaultInitialRestaurantState() {
  return {
    activeFavouriteRestaurant: null,
    favouriteRestaurantOrderType: FAVOURITE_RESTAURANT_ORDER_TYPES.ALPHABETICAL_ASCENDING,
    favouriteRestaurantArray: getFavouriteRestaurantsFromStorage(),
    favouriteRestaurantSearchTerm: "",
  };
}

function restaurantReducer(state = generateDefaultInitialRestaurantState(), action) {
  let newState;

  switch (action.type) {
    case (restaurantActionNames.ADD_FAVOURITE_RESTAURANT): {
      newState = {
        ...state,
        favouriteRestaurantArray: [
          ...state.favouriteRestaurantArray,
          action.payload.restaurant,
        ],
      };

      break;
    }

    case (restaurantActionNames.DELETE_FAVOURITE_RESTAURANT): {
      newState = {
        ...state,
        favouriteRestaurantArray: findAndRemoveFromImmutableArray(
          state.favouriteRestaurantArray,
          action.payload.restaurantId,
        ),
      };

      break;
    }

    case (restaurantActionNames.CHANGE_FAVOURITE_RESTAURANT_ORDER_TYPE): {
      newState = {
        ...state,
        favouriteRestaurantOrderType: action.payload.orderType,
      };

      break;
    }

    case (restaurantActionNames.SET_FAVOURITE_RESTAURANT_SEARCH_TERM): {
      newState = {
        ...state,
        activeFavouriteRestaurant: null,
        favouriteRestaurantSearchTerm: action.payload.searchTerm,
      };

      break;
    }

    case (restaurantActionNames.SET_ACTIVE_FAVOURITE_RESTAURANT): {
      newState = {
        ...state,
        activeFavouriteRestaurant: action.payload.restaurant,
      };

      break;
    }

    case (restaurantActionNames.RESET_ACTIVE_FAVOURITE_RESTAURANT): {
      newState = {
        ...state,
        activeFavouriteRestaurant: null,
      };

      break;
    }

    default: {
      newState = state;
    }
  }

  return newState;
}

export {generateDefaultInitialRestaurantState};
export default restaurantReducer;
