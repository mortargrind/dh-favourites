import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import {connect} from "react-redux";

import {
  validateFields,
} from "main/validation/validationUtils";
import {actionCreators as restaurantActionCreators} from "../../../restaurantActions";
import {generateRestaurantFromFormValues} from "../../../restaurantUtils";
import Button from "component/button/Button";
import TextInput from "../../../../component/text/TextInput";
import LocationSuggestionTypeAhead from "location/suggestion/LocationSuggestionTypeAhead";

import styles from "./AddFavouriteRestaurantForm.scss";

const FIELD_IDS = {
  NAME: "name",
  LOCATION: "location",
  RATING: "rating",
  FAVOURITE_FOOD: "favouriteFood",
};

const FIELD_CONFIGS = {
  [FIELD_IDS.NAME]: {
    name: "Name",
    maxLength: 140,
    validator(value) {
      const {maxLength} = FIELD_CONFIGS[FIELD_IDS.NAME];

      return (value && value.length <= maxLength);
    }
  },
  [FIELD_IDS.LOCATION]: {
    name: "Location",
    validator(value) {
      return value;
    }
  },
  [FIELD_IDS.RATING]: {
    name: "Rating",
    minValue: 1,
    maxValue: 5,
    validator(value) {
      const {minValue, maxValue} = FIELD_CONFIGS[FIELD_IDS.RATING];
      const intValue = parseInt(value, 10);

      return (intValue && intValue >= minValue && intValue <= maxValue);
    }
  },
  [FIELD_IDS.FAVOURITE_FOOD]: {
    name: "Favourite food",
    maxLength: 40,
    validator(value) {
      const {maxLength} = FIELD_CONFIGS[FIELD_IDS.FAVOURITE_FOOD];

      return (value && value.length <= maxLength);
    }
  },
};

class AddFavouriteRestaurantForm extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    addFavouriteRestaurant: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      errors: {},
      values: {
        name: "",
        rating: "",
        favouriteFood: "",
        location: null,
        visibleAddress: "",
      },
    };
  }

  locationTypeAheadRefCallback = (component) => {
    this.locationTypeAhead = component;
  };

  clearValues() {
    this.setState({
      errors: {},
      values: {
        name: "",
        rating: "",
        favouriteFood: "",
        location: null,
      }
    });

    this.locationTypeAhead.clearInputValue();
  }

  handleTextFieldChange = (event, fieldId) => {
    const {
      values: currentValues,
      errors: currentErrors,
    } = this.state;
    const newErrors = {
      ...currentErrors,
    };

    delete newErrors[fieldId];

    this.setState({
      errors: newErrors,
      values: {
        ...currentValues,
        [fieldId]: event.target.value,
      }
    });
  };

  handleLocationFieldChange = () => {
    const {errors: currentErrors} = this.state;
    const newErrors = {
      ...currentErrors,
    };

    delete newErrors[FIELD_IDS.LOCATION];

    this.setState({
      errors: newErrors,
    });
  };

  handleNameChange = (event) => {
    this.handleTextFieldChange(event, FIELD_IDS.NAME);
  };

  handleRatingChange = (event) => {
    this.handleTextFieldChange(event, FIELD_IDS.RATING);
  };

  handleFavouriteFoodChange = (event) => {
    this.handleTextFieldChange(event, FIELD_IDS.FAVOURITE_FOOD);
  };

  handleLocationSuggestionSelect = (locationSuggestion) => {
    const {values: currentValues} = this.state;

    this.setState({
      values: {
        ...currentValues,
        location: {
          address: locationSuggestion.name,
          coordinates: locationSuggestion.coordinates,
        },
      }
    });
  };

  handleResetLocationClick = () => {
    const {values: currentValues} = this.state;

    this.setState({
      values: {
        ...currentValues,
        location: null,
      }
    });

    this.locationTypeAhead.clearInputValue();
  };

  handleSubmit = (event) => {
    const {addFavouriteRestaurant} = this.props;
    const {values} = this.state;

    event.preventDefault();

    const {isValid, errors} = validateFields(values, FIELD_CONFIGS);

    if (isValid) {
      addFavouriteRestaurant(generateRestaurantFromFormValues(values));

      this.clearValues();
    } else {
      this.setState({
        errors,
      });
    }
  };

  renderLocationInput = (inputProps) => {
    const {errors} = this.state;

    return (
      <TextInput
        {...inputProps}
        isDisabled={inputProps.disabled}
        className={cn(styles.fieldInput, inputProps.className)}
        hasError={errors[FIELD_IDS.LOCATION]}
        placeholder={"Address"}/>
    );
  };

  render() {
    const {className} = this.props;
    const {
      errors,
      values: {
        name,
        rating,
        favouriteFood,
        location,
      }
    } = this.state;
    const rootClassName = cn(styles.root, className);

    return (
      <form
        className={rootClassName}
        action={"/restaurants/favourites/add"}
        onSubmit={this.handleSubmit}>

        <div className={styles.fieldContainer}>
          <LocationSuggestionTypeAhead
            ref={this.locationTypeAheadRefCallback}
            renderInput={this.renderLocationInput}
            forceRenderKey={errors[FIELD_IDS.LOCATION]}
            onInputChange={this.handleLocationFieldChange}
            onLocationSuggestionSelect={
              this.handleLocationSuggestionSelect
            }/>

          <div className={styles.fieldInfoRow}>
            <label className={styles.fieldInfoLabel}>
              {"Location: "}
            </label>
            <span className={styles.fieldInfoValue}>
              {(location && location.address) || "Not selected."}
            </span>

            {location &&
            <Button
              className={styles.clearLocationButton}
              text={"Clear"}
              onClick={this.handleResetLocationClick}/>}
          </div>
        </div>

        <div className={styles.fieldContainer}>
          <TextInput
            className={styles.fieldInput}
            hasError={errors[FIELD_IDS.NAME]}
            value={name}
            placeholder={"Name"}
            onChange={this.handleNameChange}/>
        </div>

        <div className={styles.fieldContainer}>
          <TextInput
            className={styles.fieldInput}
            hasError={errors[FIELD_IDS.RATING]}
            value={rating}
            placeholder={"Rating -/5"}
            onChange={this.handleRatingChange}/>
        </div>

        <div className={styles.fieldContainer}>
          <TextInput
            className={styles.fieldInput}
            hasError={errors[FIELD_IDS.FAVOURITE_FOOD]}
            value={favouriteFood}
            placeholder={"Favourite food"}
            onChange={this.handleFavouriteFoodChange}/>
        </div>

        <Button
          className={styles.submitButton}
          type={"submit"}
          text={"Add Your Favourite Restaurant!"}
          onClick={this.handleSubmit}/>
      </form>
    );
  }
}

const mapDispatchToProps = {
  addFavouriteRestaurant: restaurantActionCreators.addFavouriteRestaurant,
};

export default connect(null, mapDispatchToProps)(AddFavouriteRestaurantForm);
