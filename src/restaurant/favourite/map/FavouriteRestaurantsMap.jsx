import React, {PureComponent} from "react";
import {FeatureGroup, Map} from "react-leaflet";
import PropTypes from "prop-types";
import cn from "classnames";
import Leaflet from "leaflet";
import debounce from "lodash/debounce";

import MapTileLayer from "component/map/MapTileLayer";
import FavouriteRestaurantMapMarker from "./marker/FavouriteRestaurantMapMarker";

import "leaflet/dist/leaflet.css";
import styles from "./FavouriteRestaurantsMap.scss";

const COORDINATES_FOR_BERLIN = [52.5200, 13.4050]; // eslint-disable-line no-magic-numbers
const RESIZE_HANDLER_DEBOUNCE_INTERVAL = 300;
const MAP_BOUNDS_OPTIONS = {
  padding: [30, 30] // eslint-disable-line no-magic-numbers
};

class FavouriteRestaurantsMap extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    activeRestaurant: PropTypes.object,
    restaurantArray: PropTypes.array,
    singleRestaurantZoom: PropTypes.number.isRequired,
    fallbackZoom: PropTypes.number.isRequired,
    onMarkerClick: PropTypes.func,
  };

  static defaultProps = {
    singleRestaurantZoom: 8,
    fallbackZoom: 13,
  };

  constructor(props) {
    super(props);

    this.resizeHandler = debounce(this.resizeHandler, RESIZE_HANDLER_DEBOUNCE_INTERVAL);
  }

  componentDidMount() {
    window.addEventListener("resize", this.resizeHandler);

    const {leafletElement} = this.root;

    leafletElement.scrollWheelZoom.disable();

    leafletElement.on("focus", () => {
      leafletElement.scrollWheelZoom.enable();
    });

    leafletElement.on("blur", () => {
      leafletElement.scrollWheelZoom.disable();
    });
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.resizeHandler);
  }

  resizeHandler = () => {
    this.root.leafletElement.invalidateSize();
  };

  calculateMapPosition() {
    const {
      activeRestaurant,
      restaurantArray,
      fallbackZoom,
      singleRestaurantZoom,
    } = this.props;

    let center, bounds, activeZoom;

    if (!restaurantArray || restaurantArray.length === 0) {
      center = COORDINATES_FOR_BERLIN;
      activeZoom = fallbackZoom;
    } else if (activeRestaurant) {
      center = activeRestaurant.location.coordinates;
    } else if (restaurantArray.length > 1) {
      bounds = Leaflet.latLngBounds(
        restaurantArray.map(restaurant => restaurant.location.coordinates),
      );
    } else {
      center = restaurantArray[0].location.coordinates;
      activeZoom = singleRestaurantZoom;
    }

    return {
      activeZoom,
      center,
      bounds,
    };
  }

  handleMarkerClick = (event, restaurant) => {
    const {onMarkerClick} = this.props;

    if (onMarkerClick) {
      onMarkerClick(event, restaurant);
    }
  };

  refCallback = (component) => {
    this.root = component;
  };

  render() {
    const {
      className,
      activeRestaurant,
      restaurantArray,
    } = this.props;

    const rootClassName = cn(styles.root, className);
    const {activeZoom, center, bounds} = this.calculateMapPosition();

    return (
      <Map
        ref={this.refCallback}
        bounds={bounds}
        boundsOptions={MAP_BOUNDS_OPTIONS}
        center={center}
        zoom={activeZoom}
        className={rootClassName}>

        <MapTileLayer/>

        <FeatureGroup>
          {
            restaurantArray &&
            restaurantArray.map(restaurant => (
              <FavouriteRestaurantMapMarker
                key={restaurant.id}
                isActiveRestaurant={
                  activeRestaurant &&
                  activeRestaurant.id === restaurant.id
                }
                restaurant={restaurant}
                onClick={this.handleMarkerClick}/>
            ))
          }
        </FeatureGroup>
      </Map>
    );
  }
}

export default FavouriteRestaurantsMap;
