import React, {Component} from "react";
import PropTypes from "prop-types";
import {Marker} from "react-leaflet";
import Leaflet from "leaflet";

import redMarkerImage from "asset/red-map-marker-2-128.png";
import blackMarkerImage from "asset/black-map-marker-2-128.png";

const mapMarkerIconWidth = 32;
const mapMarkerIconHeight = 32;
const anchorWidthRatio = 2;

const markerIcon = Leaflet.icon({
  iconUrl: blackMarkerImage,
  iconRetinaUrl: blackMarkerImage,
  iconAnchor: [(mapMarkerIconWidth / anchorWidthRatio), mapMarkerIconHeight],
  iconSize: [mapMarkerIconWidth, mapMarkerIconHeight]
});

const activeMarkerIcon = Leaflet.icon({
  iconUrl: redMarkerImage,
  iconRetinaUrl: redMarkerImage,
  iconAnchor: [(mapMarkerIconWidth / anchorWidthRatio), mapMarkerIconHeight],
  iconSize: [mapMarkerIconWidth, mapMarkerIconHeight]
});


class FavouriteRestaurantMapMarker extends Component {
  static propTypes = {
    restaurant: PropTypes.object,
    isActiveRestaurant: PropTypes.bool,
    onClick: PropTypes.func.isRequired,
  };

  handleClick = (event) => {
    const {onClick, restaurant} = this.props;

    onClick(event, restaurant);
  };

  render() {
    const {
      restaurant,
      isActiveRestaurant,
    } = this.props;

    return (
      <Marker
        {...this.props}
        icon={isActiveRestaurant ? activeMarkerIcon : markerIcon}
        position={restaurant.location.coordinates}
        onClick={this.handleClick}/>
    );
  }
}

export default FavouriteRestaurantMapMarker;
