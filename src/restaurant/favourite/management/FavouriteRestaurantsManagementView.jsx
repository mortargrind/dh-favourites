import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import {connect} from "react-redux";
import debounce from "lodash/debounce";

import {actionCreators as restaurantActionCreators} from "restaurant/restaurantActions";
import {displayedFavouriteRestaurantArraySelector} from "restaurant/restaurantSelectors";
import TextInput from "component/text/TextInput";
import Button from "component/button/Button";
import {FAVOURITE_RESTAURANT_ORDER_TYPES} from "restaurant/restaurantConstants";
import FavouriteRestaurantRowListView from "../row/list/FavouriteRestaurantRowListView";
import FavouriteRestaurantsMap from "../map/FavouriteRestaurantsMap";

import styles from "./FavouriteRestaurantsManagementView.scss";

const FAVOURITE_RESTAURANT_SEARCH_DEBOUNCE_INTERVAL = 300;

class FavouriteRestaurantsManagementView extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    displayedFavouriteRestaurantArray: PropTypes.array,
    favouriteRestaurantOrderType: PropTypes.string.isRequired,
    totalFavouriteRestaurantCount: PropTypes.number.isRequired,
    favouriteRestaurantSearchTerm: PropTypes.string,
    activeFavouriteRestaurant: PropTypes.object,
    deleteFavouriteRestaurant: PropTypes.func.isRequired,
    changeFavouriteRestaurantOrderType: PropTypes.func.isRequired,
    setFavouriteRestaurantSearchTerm: PropTypes.func.isRequired,
    setActiveFavouriteRestaurant: PropTypes.func.isRequired,
    resetActiveFavouriteRestaurant: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      searchInputValue: "",
    };

    this.triggerFavouriteRestaurantSearch = debounce(
      this.triggerFavouriteRestaurantSearch,
      FAVOURITE_RESTAURANT_SEARCH_DEBOUNCE_INTERVAL
    );
  }

  handleDeleteFavouriteRestaurant = (restaurant) => {
    const {deleteFavouriteRestaurant} = this.props;

    deleteFavouriteRestaurant(restaurant.id);
  };

  handleListViewSortClick = () => {
    const {
      changeFavouriteRestaurantOrderType,
      favouriteRestaurantOrderType,
    } = this.props;

    let newOrderType;

    if (favouriteRestaurantOrderType === FAVOURITE_RESTAURANT_ORDER_TYPES.ALPHABETICAL_ASCENDING) {
      newOrderType = FAVOURITE_RESTAURANT_ORDER_TYPES.ALPHABETICAL_DESCENDING;
    } else {
      newOrderType = FAVOURITE_RESTAURANT_ORDER_TYPES.ALPHABETICAL_ASCENDING;
    }

    changeFavouriteRestaurantOrderType(newOrderType);
  };

  triggerFavouriteRestaurantSearch = () => {
    const {setFavouriteRestaurantSearchTerm} = this.props;
    const {searchInputValue} = this.state;

    setFavouriteRestaurantSearchTerm(searchInputValue);
  };

  generateEmptyStateMessage() {
    const {
      favouriteRestaurantSearchTerm,
      totalFavouriteRestaurantCount,
    } = this.props;

    let message;

    if (totalFavouriteRestaurantCount > 0 && favouriteRestaurantSearchTerm) {
      message = `No favourite restaurants found for '${favouriteRestaurantSearchTerm}'.`;
    } else {
      message = "No favourite restaurants found. Please add one from the top left menu.";
    }

    return message;
  }

  handleListViewSearchInputChange = (event) => {
    this.setState({
      searchInputValue: event.target.value,
    }, () => {
      this.triggerFavouriteRestaurantSearch();
    });
  };

  handleMapMarkerClick = (event, restaurant) => {
    const {setActiveFavouriteRestaurant} = this.props;

    setActiveFavouriteRestaurant(restaurant);
  };

  handleRestaurantRowClick = (restaurant) => {
    const {
      activeFavouriteRestaurant,
      setActiveFavouriteRestaurant,
      resetActiveFavouriteRestaurant
    } = this.props;

    if (activeFavouriteRestaurant && activeFavouriteRestaurant.id === restaurant.id) {
      resetActiveFavouriteRestaurant(restaurant);
    } else {
      setActiveFavouriteRestaurant(restaurant);
    }
  };

  render() {
    const {
      className,
      favouriteRestaurantOrderType,
      displayedFavouriteRestaurantArray,
      activeFavouriteRestaurant,
    } = this.props;
    const {searchInputValue} = this.state;
    const rootClassName = cn(styles.root, className);
    const isOrderTypeAscending = (
      favouriteRestaurantOrderType === FAVOURITE_RESTAURANT_ORDER_TYPES.ALPHABETICAL_ASCENDING
    );

    return (
      <div className={rootClassName}>
        <div className={styles.listViewContainer}>
          <h2 className={styles.listViewTitle}>
            {"List of Favourite Restaurants"}
          </h2>

          <div className={styles.listViewWrapper}>
            <div className={styles.listViewInputPanel}>
              <div className={styles.listViewSearchInputContainer}>
                <TextInput
                  className={styles.listViewSearchInput}
                  type={"text"}
                  placeholder={"Search by name..."}
                  value={searchInputValue}
                  onChange={this.handleListViewSearchInputChange}/>
              </div>

              <Button
                className={cn(
                  styles.listViewSortButton,
                  {
                    [styles.isAscending]: isOrderTypeAscending,
                  }
                )}
                text={"Sort By Name"}
                hasIcon={true}
                isDisabled={displayedFavouriteRestaurantArray.length === 0}
                onClick={this.handleListViewSortClick}/>
            </div>

            <FavouriteRestaurantRowListView
              className={styles.listView}
              restaurantArray={displayedFavouriteRestaurantArray}
              activeRestaurant={activeFavouriteRestaurant}
              emptyStateContent={this.generateEmptyStateMessage()}
              onRestaurantDelete={this.handleDeleteFavouriteRestaurant}
              onRowClick={this.handleRestaurantRowClick}/>
          </div>
        </div>

        <div className={styles.mapContainer}>
          <h2 className={styles.mapTitle}>
            {"Map Overview"}
          </h2>

          <div className={styles.mapWrapper}>
            <FavouriteRestaurantsMap
              className={styles.map}
              activeRestaurant={activeFavouriteRestaurant}
              restaurantArray={displayedFavouriteRestaurantArray}
              onMarkerClick={this.handleMapMarkerClick}/>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  deleteFavouriteRestaurant: restaurantActionCreators.deleteFavouriteRestaurant,
  changeFavouriteRestaurantOrderType: restaurantActionCreators.changeFavouriteRestaurantOrderType,
  setFavouriteRestaurantSearchTerm: restaurantActionCreators.setFavouriteRestaurantSearchTerm,
  setActiveFavouriteRestaurant: restaurantActionCreators.setActiveFavouriteRestaurant,
  resetActiveFavouriteRestaurant: restaurantActionCreators.resetActiveFavouriteRestaurant,
};

function mapStateToProps(state) {
  const {
    restaurantState: {
      favouriteRestaurantArray,
      favouriteRestaurantOrderType,
      activeFavouriteRestaurant,
      favouriteRestaurantSearchTerm,
    }
  } = state;

  return {
    displayedFavouriteRestaurantArray: displayedFavouriteRestaurantArraySelector(state),
    activeFavouriteRestaurant,
    favouriteRestaurantOrderType,
    favouriteRestaurantSearchTerm,
    totalFavouriteRestaurantCount: favouriteRestaurantArray.length,
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(FavouriteRestaurantsManagementView);
