import React from "react";
import renderer from "react-test-renderer";
import {shallow} from "enzyme/build";

import FavouriteRestaurantRow from "./FavouriteRestaurantRow";

/* eslint-disable no-magic-numbers, no-empty-function */

const restaurant = {
  id: "3",
  name: "Black Angus Burger",
  rating: 1,
  favouriteFood: "Cheese Burger",
  createDate: Date.now(),
  location: {
    address: "Berlin, Germany",
    coordinates: [52.5200, 13.4050],
  },
};

it("does not call delete callback on regular click", () => {
  const handleClick = jest.fn();
  const handleDelete = jest.fn();

  const wrapper = shallow(
    <FavouriteRestaurantRow
      isActive={false}
      restaurant={restaurant}
      onDelete={handleDelete}
      onClick={handleClick}/>
  );

  wrapper.simulate("click");
  expect(handleDelete.mock.calls.length).toBe(0);
  expect(handleClick.mock.calls.length).toBe(1);
});

it("does not call click callback on delete click", () => {
  const handleClick = jest.fn();
  const handleDelete = jest.fn();

  const wrapper = shallow(
    <FavouriteRestaurantRow
      isActive={false}
      restaurant={restaurant}
      onDelete={handleDelete}
      onClick={handleClick}/>
  );

  wrapper.find("Button").simulate("click", {
    stopPropagation() {}
  });
  expect(handleDelete.mock.calls.length).toBe(1);
  expect(handleClick.mock.calls.length).toBe(0);
});

it("renders correctly with a restaurant object and callbacks",
  () => {
    const handleClick = jest.fn();
    const handleDelete = jest.fn();

    const tree = renderer
      .create(
        <FavouriteRestaurantRow
          isActive={false}
          restaurant={restaurant}
          onDelete={handleDelete}
          onClick={handleClick}/>
      ).toJSON();

    expect(tree).toMatchSnapshot();
  });

it("renders correctly with a restaurant object, active state & callbacks",
  () => {
    const handleClick = jest.fn();
    const handleDelete = jest.fn();

    const tree = renderer
      .create(
        <FavouriteRestaurantRow
          isActive={true}
          restaurant={restaurant}
          onDelete={handleDelete}
          onClick={handleClick}/>
      ).toJSON();

    expect(tree).toMatchSnapshot();
  });

it("renders correctly with a restaurant object, custom class name & callbacks",
  () => {
    const handleClick = jest.fn();
    const handleDelete = jest.fn();

    const tree = renderer
      .create(
        <FavouriteRestaurantRow
          isActive={false}
          restaurant={restaurant}
          onDelete={handleDelete}
          onClick={handleClick}/>
      ).toJSON();

    expect(tree).toMatchSnapshot();
  });

it("renders correctly with a restaurant object, active state, custom class name & callbacks",
  () => {
    const handleClick = jest.fn();
    const handleDelete = jest.fn();

    const tree = renderer
      .create(
        <FavouriteRestaurantRow
          isActive={true}
          restaurant={restaurant}
          onDelete={handleDelete}
          onClick={handleClick}/>
      ).toJSON();

    expect(tree).toMatchSnapshot();
  });

/* eslint-enable no-magic-numbers, no-empty-function */
