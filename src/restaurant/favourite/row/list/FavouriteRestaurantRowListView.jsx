import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import ListView from "component/list/view/ListView";
import FavouriteRestaurantRow from "restaurant/favourite/row/FavouriteRestaurantRow";

import styles from "./FavouriteRestaurantRowListView.scss";

class FavouriteRestaurantRowListView extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    restaurantArray: PropTypes.array,
    activeRestaurant: PropTypes.object,
    isPending: PropTypes.bool,
    emptyStateContent: PropTypes.node.isRequired,
    onRestaurantDelete: PropTypes.func.isRequired,
    onRowClick: PropTypes.func,
  };

  generateListItemKey = restaurant => restaurant.id;

  renderItem = (restaurant) => {
    const {
      activeRestaurant,
      onRestaurantDelete,
      onRowClick,
    } = this.props;

    return (
      <FavouriteRestaurantRow
        restaurant={restaurant}
        isActive={
          activeRestaurant &&
          activeRestaurant.id === restaurant.id
        }
        onDelete={onRestaurantDelete}
        onClick={onRowClick}/>
    );
  };

  render() {
    const {
      className,
      restaurantArray,
      activeRestaurant,
      emptyStateContent,
      isPending,
    } = this.props;
    const rootClassName = cn(styles.root, className);

    return (
      <ListView
        forceRenderKey={activeRestaurant}
        className={rootClassName}
        listItemClassName={styles.listItem}
        emptyStateContainerClassName={styles.emptyStateContainer}
        isPending={isPending}
        renderItem={this.renderItem}
        items={restaurantArray}
        itemKeyGenerator={this.generateListItemKey}
        canDisplayEmptyState={true}
        emptyStateContent={emptyStateContent}/>
    );
  }
}

export default FavouriteRestaurantRowListView;
