import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import Button from "component/button/Button";

import styles from "./FavouriteRestaurantRow.scss";

class FavouriteRestaurantRow extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    restaurant: PropTypes.object.isRequired,
    isActive: PropTypes.bool,
    onDelete: PropTypes.func.isRequired,
    onClick: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.isAlreadyActive = false;
  }

  componentDidUpdate() {
    const {isActive} = this.props;

    if (isActive && !this.isAlreadyActive) {
      this.isAlreadyActive = true;

      this.root.scrollIntoView({
        behavior: "smooth",
      });
    }
  }

  handleDeleteButtonClick = (event) => {
    const {
      restaurant,
      onDelete,
    } = this.props;

    event.stopPropagation();

    onDelete(restaurant);
  };

  handleClick = () => {
    const {
      restaurant,
      onClick,
    } = this.props;

    if (onClick) {
      onClick(restaurant);
    }
  };

  refCallback = (element) => {
    this.root = element;
  };

  render() {
    const {
      className,
      isActive,
      restaurant,
    } = this.props;
    const rootClassName = cn(
      styles.root,
      className,
      {
        [styles.isActive]: isActive
      },
    );

    return (
      <div
        ref={this.refCallback}
        className={rootClassName}
        onClick={this.handleClick}>
        <div className={styles.info}>
          <div className={styles.name}>
            <span className={styles.infoLabel}>
              {"Name: "}
            </span>
            <span className={styles.infoValue}>
              {restaurant.name}
            </span>
          </div>

          <div className={styles.rating}>
            <span className={styles.infoLabel}>
              {"Rating: "}
            </span>
            <span className={styles.infoValue}>
              {restaurant.rating}
            </span>
          </div>

          <div className={styles.address}>
            <span className={styles.infoLabel}>
              {"Address: "}
            </span>
            <span className={styles.infoValue}>
              {restaurant.location.address}
            </span>
          </div>

          <div className={styles.favouriteFood}>
            <span className={styles.infoLabel}>
              {"Favourite Food: "}
            </span>
            <span className={styles.infoValue}>
              {restaurant.favouriteFood}
            </span>
          </div>
        </div>

        <Button
          className={styles.deleteButton}
          text={"Delete"}
          onClick={this.handleDeleteButtonClick}/>
      </div>
    );
  }
}

export default FavouriteRestaurantRow;
