const actionNames = {
  ADD_FAVOURITE_RESTAURANT: "ADD_FAVOURITE_RESTAURANT",
  DELETE_FAVOURITE_RESTAURANT: "DELETE_FAVOURITE_RESTAURANT",
  CHANGE_FAVOURITE_RESTAURANT_ORDER_TYPE: "CHANGE_FAVOURITE_RESTAURANT_ORDER_TYPE",
  SET_FAVOURITE_RESTAURANT_SEARCH_TERM: "SET_FAVOURITE_RESTAURANT_SEARCH_TERM",
  SET_ACTIVE_FAVOURITE_RESTAURANT: "SET_ACTIVE_FAVOURITE_RESTAURANT",
  RESET_ACTIVE_FAVOURITE_RESTAURANT: "RESET_ACTIVE_FAVOURITE_RESTAURANT",
};

const actionCreators = {
  addFavouriteRestaurant(restaurant) {
    return {
      type: actionNames.ADD_FAVOURITE_RESTAURANT,
      payload: {
        restaurant,
      }
    };
  },

  deleteFavouriteRestaurant(restaurantId) {
    return {
      type: actionNames.DELETE_FAVOURITE_RESTAURANT,
      payload: {
        restaurantId,
      }
    };
  },

  changeFavouriteRestaurantOrderType(orderType) {
    return {
      type: actionNames.CHANGE_FAVOURITE_RESTAURANT_ORDER_TYPE,
      payload: {
        orderType,
      },
    };
  },

  setFavouriteRestaurantSearchTerm(searchTerm) {
    return {
      type: actionNames.SET_FAVOURITE_RESTAURANT_SEARCH_TERM,
      payload: {
        searchTerm,
      },
    };
  },

  setActiveFavouriteRestaurant(restaurant) {
    return {
      type: actionNames.SET_ACTIVE_FAVOURITE_RESTAURANT,
      payload: {
        restaurant,
      },
    };
  },

  resetActiveFavouriteRestaurant() {
    return {
      type: actionNames.RESET_ACTIVE_FAVOURITE_RESTAURANT,
      payload: {},
    };
  }
};

export {
  actionNames,
  actionCreators,
};
