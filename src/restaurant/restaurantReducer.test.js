import {actionCreators as restaurantActionCreators} from "./restaurantActions";
import restaurantReducer, {
  generateDefaultInitialRestaurantState,
} from "./restaurantReducer";
import {FAVOURITE_RESTAURANT_ORDER_TYPES} from "./restaurantConstants";

let referenceTime = Date.now();

/* eslint-disable no-magic-numbers */
Date.now = function () {
  referenceTime = referenceTime + 1;

  return referenceTime;
};


const RESTAURANT_ARRAY = [
  {
    id: "1",
    name: "Burger@",
    rating: 5,
    favouriteFood: "Big Burger",
    location: {
      address: "Berlin, Germany",
      coordinates: [52.5200, 13.4050],
    },
    createDate: Date.now(),
  },
  {
    id: "2",
    name: "McDonald's",
    rating: 4,
    favouriteFood: "Big King",
    location: {
      address: "Berlin, Germany",
      coordinates: [52.5200, 13.4050],
    },
    createDate: Date.now(),
  },
  {
    id: "3",
    name: "Black Angus Burger",
    rating: 1,
    favouriteFood: "Cheese Burger",
    location: {
      address: "Berlin, Germany",
      coordinates: [52.5200, 13.4050],
    },
    createDate: Date.now(),
  },
  {
    id: "4",
    name: "Burger King",
    rating: 1,
    favouriteFood: "Whooper",
    location: {
      address: "Berlin, Germany",
      coordinates: [52.5200, 13.4050],
    },
    createDate: Date.now(),
  }
];

test("should handle 'delete favourite restaurant' action when an intermediary restaurant is deleted",
  () => {
    const favouriteRestaurantArray = RESTAURANT_ARRAY.slice(0, 3);

    console.log(favouriteRestaurantArray);

    const state = {
      ...generateDefaultInitialRestaurantState(),
      favouriteRestaurantArray,
    };

    const newState = restaurantReducer(
      state,
      restaurantActionCreators.deleteFavouriteRestaurant(
        "2"
      )
    );

    expect(newState.favouriteRestaurantArray.length).toBe(2);
    expect(newState.favouriteRestaurantArray).not.toBe(favouriteRestaurantArray);
    expect(newState.favouriteRestaurantArray[0]).toBe(favouriteRestaurantArray[0]);
    expect(newState.favouriteRestaurantArray[1]).toBe(favouriteRestaurantArray[2]);
    expect(newState.favouriteRestaurantSearchTerm).toBe(state.favouriteRestaurantSearchTerm);
    expect(newState.favouriteRestaurantOrderType).toBe(state.favouriteRestaurantOrderType);
    expect(newState.activeFavouriteRestaurant).toBe(state.activeFavouriteRestaurant);
  });

test("should handle 'delete favourite restaurant' action when first restaurant is deleted",
  () => {
    const favouriteRestaurantArray = RESTAURANT_ARRAY.slice(0, 3);

    const state = {
      ...generateDefaultInitialRestaurantState(),
      favouriteRestaurantArray,
    };
    const newState = restaurantReducer(
      state,
      restaurantActionCreators.deleteFavouriteRestaurant(
        "1"
      )
    );

    expect(newState.favouriteRestaurantArray.length).toBe(2);
    expect(newState.favouriteRestaurantArray).not.toBe(favouriteRestaurantArray);
    expect(newState.favouriteRestaurantArray[0]).toBe(favouriteRestaurantArray[1]);
    expect(newState.favouriteRestaurantArray[1]).toBe(favouriteRestaurantArray[2]);
    expect(newState.favouriteRestaurantSearchTerm).toBe(state.favouriteRestaurantSearchTerm);
    expect(newState.favouriteRestaurantOrderType).toBe(state.favouriteRestaurantOrderType);
    expect(newState.activeFavouriteRestaurant).toBe(state.activeFavouriteRestaurant);
  });

test("should handle 'delete favourite restaurant' action when last restaurant is deleted",
  () => {
    const favouriteRestaurantArray = RESTAURANT_ARRAY.slice(0, 3);

    const state = {
      ...generateDefaultInitialRestaurantState(),
      favouriteRestaurantArray,
    };
    const newState = restaurantReducer(
      state,
      restaurantActionCreators.deleteFavouriteRestaurant(
        "3"
      )
    );

    expect(newState.favouriteRestaurantArray.length).toBe(2);
    expect(newState.favouriteRestaurantArray).not.toBe(favouriteRestaurantArray);
    expect(newState.favouriteRestaurantArray[0]).toBe(favouriteRestaurantArray[0]);
    expect(newState.favouriteRestaurantArray[1]).toBe(favouriteRestaurantArray[1]);
    expect(newState.favouriteRestaurantSearchTerm).toBe(state.favouriteRestaurantSearchTerm);
    expect(newState.favouriteRestaurantOrderType).toBe(state.favouriteRestaurantOrderType);
    expect(newState.activeFavouriteRestaurant).toBe(state.activeFavouriteRestaurant);
  });

test("should handle 'add favourite restaurant' action when there are no other restaurants",
  () => {
    const favouriteRestaurantArray = [];
    const newRestaurant = RESTAURANT_ARRAY[3];

    const state = {
      ...generateDefaultInitialRestaurantState(),
      favouriteRestaurantArray,
    };
    const newState = restaurantReducer(
      state,
      restaurantActionCreators.addFavouriteRestaurant(
        newRestaurant
      )
    );

    expect(newState.favouriteRestaurantArray.length).toBe(1);
    expect(newState.favouriteRestaurantArray).not.toBe(favouriteRestaurantArray);
    expect(newState.favouriteRestaurantArray[0]).toBe(newRestaurant);
    expect(newState.favouriteRestaurantSearchTerm).toBe(state.favouriteRestaurantSearchTerm);
    expect(newState.favouriteRestaurantOrderType).toBe(state.favouriteRestaurantOrderType);
    expect(newState.activeFavouriteRestaurant).toBe(state.activeFavouriteRestaurant);
  });

test("should handle 'add favourite restaurant' action when there are other restaurants",
  () => {
    const favouriteRestaurantArray = RESTAURANT_ARRAY.slice(0, 2);
    const newRestaurant = RESTAURANT_ARRAY[3];

    const state = {
      ...generateDefaultInitialRestaurantState(),
      favouriteRestaurantArray,
    };

    const newState = restaurantReducer(
      state,
      restaurantActionCreators.addFavouriteRestaurant(
        newRestaurant
      )
    );

    expect(newState.favouriteRestaurantArray.length).toBe(3);
    expect(newState.favouriteRestaurantArray).not.toBe(favouriteRestaurantArray);
    expect(newState.favouriteRestaurantArray[0]).toEqual(favouriteRestaurantArray[0]);
    expect(newState.favouriteRestaurantArray[1]).toEqual(favouriteRestaurantArray[1]);
    expect(newState.favouriteRestaurantArray[2]).toBe(newRestaurant);
    expect(newState.favouriteRestaurantSearchTerm).toBe(state.favouriteRestaurantSearchTerm);
    expect(newState.favouriteRestaurantOrderType).toBe(state.favouriteRestaurantOrderType);
    expect(newState.activeFavouriteRestaurant).toBe(state.activeFavouriteRestaurant);
  });

test("should handle 'change order type' action correctly",
  () => {
    const favouriteRestaurantArray = [...RESTAURANT_ARRAY.slice(2, 4)];
    const state = {
      ...generateDefaultInitialRestaurantState(),
      favouriteRestaurantArray,
      favouriteRestaurantOrderType: FAVOURITE_RESTAURANT_ORDER_TYPES.ALPHABETICAL_ASCENDING,
    };

    const newState = restaurantReducer(
      state,
      restaurantActionCreators.changeFavouriteRestaurantOrderType(
        FAVOURITE_RESTAURANT_ORDER_TYPES.ALPHABETICAL_DESCENDING
      )
    );

    expect(newState.favouriteRestaurantOrderType).toBe(FAVOURITE_RESTAURANT_ORDER_TYPES.ALPHABETICAL_DESCENDING);
    expect(newState.favouriteRestaurantArray).toBe(state.favouriteRestaurantArray);
    expect(newState.favouriteRestaurantSearchTerm).toBe(state.favouriteRestaurantSearchTerm);
    expect(newState.activeFavouriteRestaurant).toBe(state.activeFavouriteRestaurant);
  });

test("should handle 'set search term' action correctly when there's an active favourite restaurant",
  () => {
    const favouriteRestaurantArray = RESTAURANT_ARRAY.slice(0, 3);
    const state = {
      ...generateDefaultInitialRestaurantState(),
      favouriteRestaurantArray,
      activeFavouriteRestaurant: {
        id: "3",
        name: "Black Angus Burger",
        rating: 1,
        favouriteFood: "Cheese Burger",
        location: {
          address: "Berlin, Germany",
          coordinates: [52.5200, 13.4050],
        },
        createDate: Date.now(),
      },
      favouriteRestaurantSearchTerm: "hey",
    };

    const newState = restaurantReducer(
      state,
      restaurantActionCreators.setFavouriteRestaurantSearchTerm(
        "hoy"
      )
    );

    expect(newState.favouriteRestaurantSearchTerm).toBe("hoy");
    expect(newState.favouriteRestaurantArray).toBe(state.favouriteRestaurantArray);
    expect(newState.favouriteRestaurantOrderType).toBe(state.favouriteRestaurantOrderType);
    expect(newState.activeFavouriteRestaurant).toBe(null);
  });

test("should handle 'set search term' action correctly when there's no favourite restaurant",
  () => {
    const favouriteRestaurantArray = RESTAURANT_ARRAY.slice(0, 3);
    const state = {
      ...generateDefaultInitialRestaurantState(),
      favouriteRestaurantArray,
      activeFavouriteRestaurant: null,
      favouriteRestaurantSearchTerm: "hey",
    };

    const newState = restaurantReducer(
      state,
      restaurantActionCreators.setFavouriteRestaurantSearchTerm(
        "hoy"
      )
    );

    expect(newState.favouriteRestaurantSearchTerm).toBe("hoy");
    expect(newState.favouriteRestaurantArray).toBe(state.favouriteRestaurantArray);
    expect(newState.favouriteRestaurantOrderType).toBe(state.favouriteRestaurantOrderType);
    expect(newState.activeFavouriteRestaurant).toBe(null);
  });

test("should handle 'set active favourite restaurant' action correctly when there's no favourite restaurant",
  () => {
    const favouriteRestaurantArray = RESTAURANT_ARRAY.slice(0, 3);

    const state = {
      ...generateDefaultInitialRestaurantState(),
      favouriteRestaurantArray,
      activeFavouriteRestaurant: null,
      favouriteRestaurantSearchTerm: "hey",
    };

    const newActiveFavouriteRestaurant = {
      id: "3",
      name: "Black Angus Burger",
      rating: 1,
      favouriteFood: "Cheese Burger",
      createDate: Date.now(),
    };

    const newState = restaurantReducer(
      state,
      restaurantActionCreators.setActiveFavouriteRestaurant(
        newActiveFavouriteRestaurant,
      )
    );

    expect(newState.favouriteRestaurantSearchTerm).toBe(state.favouriteRestaurantSearchTerm);
    expect(newState.favouriteRestaurantArray).toBe(state.favouriteRestaurantArray);
    expect(newState.favouriteRestaurantOrderType).toBe(state.favouriteRestaurantOrderType);
    expect(newState.activeFavouriteRestaurant).toBe(newActiveFavouriteRestaurant);
  });

test("should handle 'set active favourite restaurant' action correctly when there's an active favourite restaurant",
  () => {
    const favouriteRestaurantArray = RESTAURANT_ARRAY.slice(0, 3);

    const state = {
      ...generateDefaultInitialRestaurantState(),
      favouriteRestaurantArray,
      activeFavouriteRestaurant: {
        id: "4",
        name: "Burger King",
        rating: 1,
        favouriteFood: "Whooper",
        createDate: Date.now(),
      },
      favouriteRestaurantSearchTerm: "hey",
    };

    const newActiveFavouriteRestaurant = {
      id: "3",
      name: "Black Angus Burger",
      rating: 1,
      favouriteFood: "Cheese Burger",
      createDate: Date.now(),
    };

    const newState = restaurantReducer(
      state,
      restaurantActionCreators.setActiveFavouriteRestaurant(
        newActiveFavouriteRestaurant,
      )
    );

    expect(newState.favouriteRestaurantSearchTerm).toBe(state.favouriteRestaurantSearchTerm);
    expect(newState.favouriteRestaurantArray).toBe(state.favouriteRestaurantArray);
    expect(newState.favouriteRestaurantOrderType).toBe(state.favouriteRestaurantOrderType);
    expect(newState.activeFavouriteRestaurant).toBe(newActiveFavouriteRestaurant);
  });
/* eslint-enable no-magic-numbers */
