import camelCase from "lodash/camelCase";
import upperFirst from "lodash/upperFirst";

function toUpperFirst(string) {
  return upperFirst(string);
}

function toCamelCase(string) {
  return camelCase(string);
}

export {
  toCamelCase,
  toUpperFirst,
};
