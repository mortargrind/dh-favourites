/* eslint-disable no-magic-numbers */
import {
  findAndRemoveFromImmutableArray,
} from "./collectionUtils";

test("findAndRemoveFromImmutableArray correctly removes an item with the default config",
  () => {
    const array = [
      {
        id: "1",
        name: "Jon"
      },
      {
        id: "2",
        name: "Stannis"
      },
      {
        id: "3",
        name: "Davos"
      }
    ];
    const result = findAndRemoveFromImmutableArray(array, "2");

    expect(result.length)
      .toBe(2);
    expect(result[0].name)
      .toBe("Jon");
    expect(result[1].name)
      .toBe("Davos");
  });

test("findAndRemoveFromImmutableArray correctly removes an item with a custom propName",
  () => {
    const array = [
      {
        id: "1",
        name: "Jon"
      },
      {
        id: "2",
        name: "Stannis"
      },
      {
        id: "3",
        name: "Davos"
      }
    ];
    const result = findAndRemoveFromImmutableArray(array, "Davos", {
      propName: "name"
    });

    expect(result.length)
      .toBe(2);
    expect(result[0].name)
      .toBe("Jon");
    expect(result[1].name)
      .toBe("Stannis");
  });
/* eslint-enable no-magic-numbers */
