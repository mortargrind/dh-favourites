import findIndex from "lodash/findIndex";

function filterNonExistentObjectValues(
  object,
  {shouldFilterEmptyStrings = false} = {}
) {
  const filteredObject = {};

  for (const key in object) {
    if (Object.prototype.hasOwnProperty.call(object, key)) {
      const value = object[key];

      if (value !== null &&
        typeof value !== "undefined" &&
        (!shouldFilterEmptyStrings || value !== "")) {
        filteredObject[key] = value;
      }
    }
  }

  return filteredObject;
}

const defaultFindAndRemoveFromImmutableArrayConfig = {
  propName: "id",
  extractor: (item, config) => item[config.propName],
};

function findAndRemoveFromImmutableArray(
  originalArray,
  property,
  config = {}) {
  const finalConfig = {
    ...defaultFindAndRemoveFromImmutableArrayConfig,
    ...config
  };
  const deletedItemIndex = findIndex(
    originalArray,
    item => finalConfig.extractor(item, finalConfig) === property
  );

  return originalArray.slice(0, deletedItemIndex)
    .concat(
      originalArray.slice(deletedItemIndex + 1)
    );
}

export {
  filterNonExistentObjectValues,
  findAndRemoveFromImmutableArray,
};
