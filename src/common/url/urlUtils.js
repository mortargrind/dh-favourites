import qs from "qs";

import {
  QUERY_STRING_PREFIX,
} from "./urlConstants";
import {
  filterNonExistentObjectValues,
} from "../collection/collectionUtils";

function generateQueryString(params,
  {
    withPrefix = true,
    ...queryStringLibOptions
  } = {}) {
  let result = qs.stringify(
    filterNonExistentObjectValues(
      params,
      {
        shouldFilterEmptyStrings: true,
      },
    ),
    queryStringLibOptions,
  );

  if (result && withPrefix) {
    result = QUERY_STRING_PREFIX + result;
  }

  return result;
}


export {
  generateQueryString,
};
