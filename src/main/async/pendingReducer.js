import {
  toCamelCase,
  toUpperFirst,
} from "common/string/stringUtils";
import {stripAsyncActionNameSuffixes} from "main/async/asyncReduxUtils";
import {
  PENDING_FLAG_PREFIX,
  PENDING_FLAG_SUFFIX,
} from "main/async/asyncReduxConstants";

function generateDefaultInitialPendingState() {
  return {
    isSearchLocationSuggestionPending: false,
  };
}

function pendingReducer(state = generateDefaultInitialPendingState(), action) {
  let newState;

  if (action.isAsync && action.pendingResult != null) {
    const {baseActionName} = stripAsyncActionNameSuffixes(action.type);
    const pendingFlagName = PENDING_FLAG_PREFIX + toUpperFirst(toCamelCase(baseActionName)) + PENDING_FLAG_SUFFIX;

    newState = {
      ...state,
      [pendingFlagName]: action.pendingResult,
    };
  } else {
    newState = state;
  }

  return newState;
}

export {generateDefaultInitialPendingState};
export default pendingReducer;
