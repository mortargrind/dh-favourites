import {
  take,
  fork,
  cancel as cancelEffect,
  call,
  put,
  cancelled as cancelledEffect,
} from "redux-saga/effects";

import {
  ACTION_NAME_DELIMITER,
  ASYNC_ACTION_SUFFIXES,
  ASYNC_ACTION_TYPES,
  ASYNC_ACTION_PHASES,
} from "./asyncReduxConstants";

function stripAsyncActionNameSuffixes(actionName) {
  const partials = actionName.split(ACTION_NAME_DELIMITER);
  const suffix = partials[partials.length - 1];
  const hasAsyncSuffix =
    suffix === ASYNC_ACTION_SUFFIXES.FULFILLED ||
    suffix === ASYNC_ACTION_SUFFIXES.REJECTED ||
    suffix === ASYNC_ACTION_SUFFIXES.CANCEL ||
    suffix === ASYNC_ACTION_SUFFIXES.CLEANUP;

  let baseActionName;

  if (hasAsyncSuffix) {
    baseActionName = partials.slice(0, partials.length - 1).join(ACTION_NAME_DELIMITER);
  } else {
    baseActionName = actionName;
  }

  return {
    baseActionName,
    suffix,
  };
}

function generateAsyncActionNames(baseActionName) {
  return {
    triggerActionName: baseActionName,
    fulfilledActionName: baseActionName + ACTION_NAME_DELIMITER + ASYNC_ACTION_SUFFIXES.FULFILLED,
    rejectedActionName: baseActionName + ACTION_NAME_DELIMITER + ASYNC_ACTION_SUFFIXES.REJECTED,
    cancelActionName: baseActionName + ACTION_NAME_DELIMITER + ASYNC_ACTION_SUFFIXES.CANCEL,
    cleanupActionName: baseActionName + ACTION_NAME_DELIMITER + ASYNC_ACTION_SUFFIXES.CLEANUP,
  };
}

function generateAsyncActionNameConstants(baseActionName) {
  const actionNames = generateAsyncActionNames(baseActionName);

  return {
    TRIGGER: actionNames.triggerActionName,
    FULFILLED: actionNames.fulfilledActionName,
    REJECTED: actionNames.rejectedActionName,
    CANCEL: actionNames.cancelActionName,
    CLEANUP: actionNames.cleanupActionName,
  };
}

function generateBasicAsyncWatcher(baseActionName, asyncSaga) {
  const {
    triggerActionName,
    fulfilledActionName,
    rejectedActionName,
    cancelActionName,
  } = generateAsyncActionNames(baseActionName);

  return function* () {
    while (true) {
      const triggerAction = yield take(triggerActionName);
      const task = yield fork(asyncSaga, triggerAction);
      const resultAction = yield take([
        triggerActionName,
        fulfilledActionName,
        rejectedActionName,
        cancelActionName,
      ]);

      if (resultAction.type === cancelActionName) {
        yield cancelEffect(task);
      }
    }
  };
}

function generateBasicAsyncActionCreators({
  baseActionName,
  triggerPayloadGenerator,
}) {
  const asyncType = ASYNC_ACTION_TYPES.BASIC;
  const {
    triggerActionName,
    fulfilledActionName,
    rejectedActionName,
    cancelActionName,
    cleanupActionName,
  } = generateAsyncActionNames(baseActionName);

  function trigger(...args) {
    return {
      type: triggerActionName,
      payload: triggerPayloadGenerator ? triggerPayloadGenerator(...args) : {},
      isAsync: true,
      asyncType,
      asyncPhase: ASYNC_ACTION_PHASES.TRIGGER,
      pendingResult: true,
    };
  }

  function fulfill(response, triggerPayload) {
    return {
      type: fulfilledActionName,
      payload: {
        response,
      },
      triggerPayload,
      isAsync: true,
      asyncType,
      asyncPhase: ASYNC_ACTION_PHASES.FULFILL,
      pendingResult: false,
    };
  }

  function reject(error, triggerPayload) {
    return {
      type: rejectedActionName,
      payload: {
        error,
      },
      triggerPayload,
      isError: true,
      isAsync: true,
      asyncType,
      asyncPhase: ASYNC_ACTION_PHASES.REJECT,
      pendingResult: false,
    };
  }

  function cancel(triggerPayload) {
    return {
      type: cancelActionName,
      triggerPayload,
      isAsync: true,
      asyncType,
      asyncPhase: ASYNC_ACTION_PHASES.CANCEL,
      payload: {},
    };
  }

  function cleanup(triggerPayload) {
    return {
      type: cleanupActionName,
      payload: {},
      triggerPayload,
      isAsync: true,
      asyncType,
      asyncPhase: ASYNC_ACTION_PHASES.CLEANUP,
      pendingResult: false,
    };
  }

  return {
    trigger,
    fulfill,
    reject,
    cancel,
    cleanup,
  };
}

function generateBasicAsyncSaga(
  asyncOperation,
  actionCreators,
  payloadExtractor = () => []) {
  return function* (action) {
    const {payload} = action;

    let result;

    try {
      const response = yield call(asyncOperation, ...payloadExtractor(payload));

      yield put(actionCreators.fulfill(response, payload));
      result = response;
    } catch (error) {
      yield put(actionCreators.reject(error, payload));
      result = error;
    } finally {
      if (yield cancelledEffect()) {
        yield put(actionCreators.cleanup(payload));
      }
    }

    return result;
  };
}

function generateBasicAsyncWatcherAndWorkerSagas({
  baseActionName,
  actionCreators,
  asyncOperation,
  payloadExtractor
}) {
  const workerSaga = generateBasicAsyncSaga(asyncOperation, actionCreators, payloadExtractor);

  return {
    watcherSaga: generateBasicAsyncWatcher(baseActionName, workerSaga),
    workerSaga,
  };
}

export {
  stripAsyncActionNameSuffixes,
  generateBasicAsyncWatcher,
  generateAsyncActionNames,
  generateAsyncActionNameConstants,
  generateBasicAsyncActionCreators,
  generateBasicAsyncSaga,
  generateBasicAsyncWatcherAndWorkerSagas,
};
