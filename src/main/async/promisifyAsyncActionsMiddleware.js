import {
  ASYNC_ACTION_PHASES,
  ASYNC_ACTION_TYPES,
} from "./asyncReduxConstants";

function promisifyAsyncActionsMiddleware() {
  const promiseContextMap = new WeakMap();

  return function (next) {
    return function (action) {
      let result;

      if (action.isAsync && action.asyncType === ASYNC_ACTION_TYPES.BASIC) {
        if (action.asyncPhase === ASYNC_ACTION_PHASES.TRIGGER) {
          next(action);

          const promiseContext = {};
          const promise = new Promise((resolve, reject) => {
            promiseContext.resolve = resolve;
            promiseContext.reject = reject;
          });

          promiseContext.promise = promise;
          promiseContextMap.set(action.payload, promiseContext);

          result = promise;
        } else {
          const promiseContext = promiseContextMap.get(action.triggerPayload);

          if (promiseContext) {
            if (action.asyncPhase === ASYNC_ACTION_PHASES.FULFILL) {
              promiseContext.resolve(action.payload.response);
            } else if (action.asyncPhase === ASYNC_ACTION_PHASES.REJECT) {
              promiseContext.reject(action.payload.error);
            }

            promiseContextMap.delete(action.triggerPayload);
          }

          result = next(action);
        }
      } else {
        result = next(action);
      }

      return result;
    };
  };
}

export default promisifyAsyncActionsMiddleware;
