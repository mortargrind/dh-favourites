import {
  HTTP_CLIENT_ERROR_STATUS_CODE_PREFIX,
  HTTP_SERVER_ERROR_STATUS_CODE_PREFIX
} from "./networkConstants";

function checkForErrorResponse(response) {
  const {status} = response;
  const statusCodeString = status.toString();

  return (statusCodeString.startsWith(HTTP_CLIENT_ERROR_STATUS_CODE_PREFIX) ||
    statusCodeString.startsWith(HTTP_SERVER_ERROR_STATUS_CODE_PREFIX));
}

export {
  checkForErrorResponse,
};
