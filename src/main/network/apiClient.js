import {
  CONTENT_TYPE_HEADER,
  JSON_CONTENT_TYPE,
  HTTP_METHODS, HTTP_STATUS_CODES,
} from "./networkConstants";
import {checkForErrorResponse} from "./networkUtils";
import {generateQueryString} from "common/url/urlUtils";

const defaultApiConfig = {
  mode: "cors",
  cache: "no-cache",
  credentials: "same-origin",
  headers: {
    [CONTENT_TYPE_HEADER]: JSON_CONTENT_TYPE,
  },
  redirect: "follow",
  referrer: "no-referrer",
};

function createApiClient(apiConfig = {}) {
  function sendRequest({
    method,
    url,
    query,
    body,
    config: requestConfig
  }) {
    let requestUrl;

    const finalConfig = {
      ...apiConfig,
      ...requestConfig,
    };

    const {baseUrlPrefix} = finalConfig;

    if (baseUrlPrefix) {
      requestUrl = baseUrlPrefix + url;
    } else {
      requestUrl = url;
    }

    requestUrl += generateQueryString(query);

    return window
      .fetch(requestUrl, {
        ...apiConfig,
        ...requestConfig,
        method,
        body: JSON.stringify(body),
      })
      .then(response => response.json()
        .then(data => ({
          status: response.status,
          data
        })))
      .catch((error) => {
        console.error(error);

        return {
          status: HTTP_STATUS_CODES.INTERNAL_SERVER_ERROR,
          data: {},
        };
      })
      .then((parsedResponse) => {
        if (checkForErrorResponse(parsedResponse)) {
          throw parsedResponse;
        } else {
          return parsedResponse;
        }
      });
  }

  return {
    get({url, query, config}) {
      return sendRequest({
        method: HTTP_METHODS.GET,
        url,
        query,
        config,
      });
    },

    post({url, query, body, config}) {
      return sendRequest({
        method: HTTP_METHODS.POST,
        url,
        body,
        query,
        config,
      });
    },

    put({url, query, body, config}) {
      return sendRequest({
        method: HTTP_METHODS.PUT,
        url,
        body,
        query,
        config,
      });
    },

    delete({url, query, config}) {
      return sendRequest({
        method: HTTP_METHODS.DELETE,
        url,
        query,
        config,
      });
    },

    head({url, query, config}) {
      return sendRequest({
        method: HTTP_METHODS.HEAD,
        url,
        query,
        config,
      });
    },

    options({url, query, config}) {
      return sendRequest({
        method: HTTP_METHODS.OPTIONS,
        url,
        query,
        requestConfig: config,
      });
    },
  };
}

export {defaultApiConfig};
export default createApiClient;
