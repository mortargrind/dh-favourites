function validateFields(values, fieldConfigs) {
  let isValid = true;
  const errors = {};

  Array.from(Object.entries(fieldConfigs))
    .forEach(([fieldId, fieldConfig]) => {
      if (!fieldConfig.validator(values[fieldId])) {
        isValid = false;
        errors[fieldId] = true;
      }
    });

  return {
    isValid,
    errors,
  };
}

export {
  validateFields,
};
