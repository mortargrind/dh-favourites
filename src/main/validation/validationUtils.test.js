import {
  validateFields,
} from "./validationUtils";

const FIELD_IDS = {
  FIRST_NAME: "firstName",
  LAST_NAME: "lastName",
  AGE: "age",
};

const FIELD_CONFIGS = {
  [FIELD_IDS.FIRST_NAME]: {
    name: "First Name",
    maxLength: 20,
    validator(value) {
      const {maxLength} = FIELD_CONFIGS[FIELD_IDS.FIRST_NAME];

      return (value && value.length <= maxLength);
    }
  },
  [FIELD_IDS.LAST_NAME]: {
    name: "Last Name",
    maxLength: 20,
    validator(value) {
      const {maxLength} = FIELD_CONFIGS[FIELD_IDS.LAST_NAME];

      return (value && value.length <= maxLength);
    }
  },
  [FIELD_IDS.AGE]: {
    name: "Age",
    minValue: 0,
    maxValue: 99,
    validator(value) {
      const {minValue, maxValue} = FIELD_CONFIGS[FIELD_IDS.AGE];
      const intValue = parseInt(value, 10);

      return (intValue && intValue >= minValue && intValue <= maxValue);
    }
  },
};

test("should validate fields correctly if all of them are invalid", () => {
  const values = {
    [FIELD_IDS.FIRST_NAME]: "1234567890123456789123456",
    [FIELD_IDS.LAST_NAME]: "123456789012345678912345612121",
    [FIELD_IDS.AGE]: -1,
  };

  const {
    isValid,
    errors,
  } = validateFields(values, FIELD_CONFIGS);

  expect(isValid).toBe(false);
  expect(errors).toStrictEqual({
    firstName: true,
    lastName: true,
    age: true,
  });
});

test("should validate fields correctly if all of them are valid", () => {
  const values = {
    [FIELD_IDS.FIRST_NAME]: "John",
    [FIELD_IDS.LAST_NAME]: "Snow",
    [FIELD_IDS.AGE]: 24,
  };

  const {
    isValid,
    errors,
  } = validateFields(values, FIELD_CONFIGS);

  expect(isValid).toBe(true);
  expect(errors).toStrictEqual({});
});

test("should validate fields correctly if some of them are valid", () => {
  const values = {
    [FIELD_IDS.FIRST_NAME]: "John",
    [FIELD_IDS.LAST_NAME]: "Snow",
    [FIELD_IDS.AGE]: -24,
  };

  const {
    isValid,
    errors,
  } = validateFields(values, FIELD_CONFIGS);

  expect(isValid).toBe(false);
  expect(errors).toStrictEqual({
    age: true,
  });
});
