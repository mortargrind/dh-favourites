import {
  generateAsyncActionNameConstants,
  generateBasicAsyncActionCreators,
} from "main/async/asyncReduxUtils";

const GET_LOCATION_SUGGESTIONS_BASE_ACTION_NAME = "GET_LOCATION_SUGGESTIONS";
const GET_LOCATION_SUGGESTIONS_ACTION_NAMES = generateAsyncActionNameConstants(
  GET_LOCATION_SUGGESTIONS_BASE_ACTION_NAME
);

const actionNames = {
  GET_LOCATION_SUGGESTIONS_BASE_ACTION_NAME,
  GET_LOCATION_SUGGESTIONS: {
    TRIGGER: GET_LOCATION_SUGGESTIONS_ACTION_NAMES.TRIGGER,
    FULFILLED: GET_LOCATION_SUGGESTIONS_ACTION_NAMES.FULFILLED,
    REJECTED: GET_LOCATION_SUGGESTIONS_ACTION_NAMES.REJECTED,
    CANCEL: GET_LOCATION_SUGGESTIONS_ACTION_NAMES.CANCEL,
    CLEANUP: GET_LOCATION_SUGGESTIONS_ACTION_NAMES.CLEANUP,
  },
};

const getLocationSuggestionsActionCreators = generateBasicAsyncActionCreators({
  baseActionName: GET_LOCATION_SUGGESTIONS_BASE_ACTION_NAME,
  triggerPayloadGenerator: searchTerm => ({
    searchTerm,
  })
});

const actionCreators = {
  getLocationSuggestions: {
    ...getLocationSuggestionsActionCreators,
  }
};

export {
  actionNames,
  actionCreators,
  getLocationSuggestionsActionCreators,
};
