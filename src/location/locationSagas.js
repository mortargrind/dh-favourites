import {generateBasicAsyncWatcherAndWorkerSagas} from "main/async/asyncReduxUtils";
import {
  actionNames as locationActionNames,
  actionCreators as locationActionCreators,
} from "location/locationActions";
import {locationApiFactory} from "location/locationApi";

function locationSagasFactory({mapboxApiClient}) {
  const locationApi = locationApiFactory({
    mapboxApiClient
  });

  const {watcherSaga: watchGetLocationSuggestions} = generateBasicAsyncWatcherAndWorkerSagas({
    baseActionName: locationActionNames.GET_LOCATION_SUGGESTIONS_BASE_ACTION_NAME,
    actionCreators: locationActionCreators.getLocationSuggestions,
    asyncOperation: locationApi.getLocationSuggestions,
    payloadExtractor: payload => [payload.searchTerm],
  });

  const locationSagas = [
    watchGetLocationSuggestions(),
  ];

  return locationSagas;
}

export {
  locationSagasFactory,
};
