import {
  actionNames as locationActionNames,
} from "location/locationActions";

function generateDefaultInitialLocationState() {
  return {
    locationSuggestionArray: null,
  };
}

function locationReducer(state = generateDefaultInitialLocationState(), action) {
  let newState;

  switch (action.type) {
    case (locationActionNames.GET_LOCATION_SUGGESTIONS.FULFILLED): {
      newState = {
        ...state,
        locationSuggestionArray: action.payload.response.data.features.map(
          feature => ({
            id: feature.id,
            name: feature.place_name,
            coordinates: [
              ...feature.geometry.coordinates.reverse(),
            ]
          })
        )
      };

      break;
    }

    default: {
      newState = state;
    }
  }

  return newState;
}

export {generateDefaultInitialLocationState};
export default locationReducer;

