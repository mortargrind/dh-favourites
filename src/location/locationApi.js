function locationApiFactory({mapboxApiClient}) {
  return {
    getLocationSuggestions(searchTerm) {
      return mapboxApiClient.get({
        url: `/geocoding/v5/mapbox.places/${searchTerm}.json`,
        query: {
          access_token: process.env.DH_MAPBOX_ACCESS_TOKEN,
        }
      });
    }
  };
}

export {
  locationApiFactory,
};
