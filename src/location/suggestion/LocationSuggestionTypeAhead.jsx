import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import debounce from "lodash/debounce";

import {actionCreators as locationActionCreators} from "location/locationActions";
import TypeAhead from "component/type-ahead/TypeAhead";
import LocationSuggestionRow from "location/suggestion/row/LocationSuggestionRow";

const FETCH_LOCATION_SUGGESTION_DEBOUNCE_INTERVAL = 300;

class LocationSuggestionTypeAhead extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    locationSuggestionArray: PropTypes.array,
    inputPlaceholder: PropTypes.string,
    triggerGetLocationSuggestions: PropTypes.func.isRequired,
    isGetLocationSuggestionsPending: PropTypes.bool,
    onLocationSuggestionSelect: PropTypes.func,
    onInputChange: PropTypes.func,
  }

  constructor(props) {
    super(props);

    this.state = {
      inputValue: "",
    };

    this.fetchLocationSuggestions = debounce(
      this.fetchLocationSuggestions,
      FETCH_LOCATION_SUGGESTION_DEBOUNCE_INTERVAL,
    );
  }

  fetchLocationSuggestions = (searchTerm) => {
    const {triggerGetLocationSuggestions} = this.props;

    triggerGetLocationSuggestions(searchTerm);
  }

  handleInputChange = (event) => {
    const {onInputChange} = this.props;

    this.setState({
      inputValue: event.target.value,
    });

    if (onInputChange) {
      onInputChange(event);
    }

    this.fetchLocationSuggestions(event.target.value);
  }

  handleLocationSuggestionSelect = (locationSuggestion) => {
    const {onLocationSuggestionSelect} = this.props;

    this.setState({
      inputValue: locationSuggestion.name,
    });

    onLocationSuggestionSelect(locationSuggestion);
  }

  generateResultId = locationSuggestion => locationSuggestion.id

  clearInputValue = () => {
    this.setState({
      inputValue: "",
    });
  }

  renderLocationSuggestionRow = locationSuggestion => (
    <LocationSuggestionRow locationSuggestion={locationSuggestion}/>
  )

  render() {
    const {
      locationSuggestionArray,
      isGetLocationSuggestionsPending,
    } = this.props;
    const {inputValue} = this.state;

    return (
      <TypeAhead
        {...this.props}
        results={locationSuggestionArray}
        inputValue={inputValue}
        isPending={isGetLocationSuggestionsPending}
        renderResult={this.renderLocationSuggestionRow}
        resultIdGenerator={this.generateResultId}
        onResultSelect={this.handleLocationSuggestionSelect}
        onInputChange={this.handleInputChange}/>
    );
  }
}

const mapDispatchToProps = {
  triggerGetLocationSuggestions: locationActionCreators.getLocationSuggestions.trigger,
  cancelGetLocationSuggestions: locationActionCreators.getLocationSuggestions.cancel,
};

function mapStateToProps(state) {
  const {
    pendingState: {isGetLocationSuggestionsPending},
    locationState: {locationSuggestionArray}
  } = state;

  return {
    isGetLocationSuggestionsPending,
    locationSuggestionArray,
  };
}

export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true
})(LocationSuggestionTypeAhead);
