import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import styles from "./LocationSuggestionResultRow.scss";

class LocationSuggestionRow extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    locationSuggestion: PropTypes.object.isRequired,
  }

  render() {
    const {
      className,
      locationSuggestion,
    } = this.props;
    const rootClassName = cn(styles.root, className);

    return (
      <div className={rootClassName}>
        {locationSuggestion.name}
      </div>
    );
  }
}

export default LocationSuggestionRow;
