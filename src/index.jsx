import "whatwg-fetch";
import React from "react";
import ReactDOM from "react-dom";
import {Provider as ReduxProvider} from "react-redux";

import "./main/ui/base/module.scss";
import {createReduxStore} from "./app/appReduxStoreUtils";
import App from "./app/App";

const reduxStore = createReduxStore({});
const appContainerElement = document.getElementById("root");

function run() {
  ReactDOM.render(
    <ReduxProvider store={reduxStore}>
      <App/>
    </ReduxProvider>,
    appContainerElement,
  );
}

run();
