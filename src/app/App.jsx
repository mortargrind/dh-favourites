import React, {PureComponent} from "react";
import {connect} from "react-redux";
import {hot} from "react-hot-loader/root";
import cn from "classnames";

import Button from "component/button/Button";
import AddFavouriteRestaurantForm from "restaurant/favourite/add/form/AddFavouriteRestaurantForm";
import FavouriteRestaurantsManagementView from "restaurant/favourite/management/FavouriteRestaurantsManagementView";

import styles from "./App.scss";

import dhLogoImage from "asset/logo/dh-logo.png";

class App extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isSideBarVisible: false
    };
  }

  handleHideSidebarClick = () => {
    this.setState({
      isSideBarVisible: false,
    });
  };

  handleDisplaySidebarClick = () => {
    this.setState({
      isSideBarVisible: true,
    });
  };

  handleSidebarOverlayClick = () => {
    this.setState({
      isSideBarVisible: false,
    });
  };

  render() {
    const {isSideBarVisible} = this.state;
    const rootClassName = cn(styles.root, {
      [styles.isSideBarVisible]: isSideBarVisible,
    });

    return (
      <div className={rootClassName}>
        <header className={styles.header}>
          <Button
            className={styles.displaySideBarButton}
            hasIcon={true}
            onClick={this.handleDisplaySidebarClick}/>

          <img
            className={styles.headerLogo}
            src={dhLogoImage}/>
        </header>

        <div className={styles.body}>
          <div className={styles.contentWrapper}>
            <div className={styles.sidebar}>
              <AddFavouriteRestaurantForm/>

              <Button
                className={styles.hideSideBarButton}
                hasIcon={true}
                onClick={this.handleHideSidebarClick}/>
            </div>

            {isSideBarVisible &&
            <div
              className={styles.sidebarOverlay}
              onClick={this.handleSidebarOverlayClick}/>}

            <div className={styles.content}>
              <FavouriteRestaurantsManagementView
                className={styles.managementView}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    state,
  };
}


export default hot(connect(mapStateToProps)(App));
