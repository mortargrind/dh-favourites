import {combineReducers} from "redux";

import pendingReducer, {
  generateDefaultInitialPendingState,
} from "main/async/pendingReducer";
import restaurantReducer, {
  generateDefaultInitialRestaurantState,
} from "restaurant/restaurantReducer";
import locationReducer, {
  generateDefaultInitialLocationState,
} from "location/locationReducer";

function createInitialReduxStoreState(customInitialState) {
  const initialState = {
    restaurantState: {
      ...generateDefaultInitialRestaurantState(),
      ...customInitialState.restaurantState,
    },
    locationState: {
      ...generateDefaultInitialLocationState(),
      ...customInitialState.locationState,
    },
    pendingState: {
      ...generateDefaultInitialPendingState(),
      ...customInitialState.pendingState,
    },
  };

  return initialState;
}

function createAppReducer() {
  const reducers = {
    pendingState: pendingReducer,
    restaurantState: restaurantReducer,
    locationState: locationReducer,
  };

  return combineReducers(reducers);
}

export {
  createAppReducer,
  createInitialReduxStoreState,
};
