import {all} from "redux-saga/effects";

import {
  locationSagasFactory,
} from "location/locationSagas";
import {
  restaurantSagasFactory,
} from "restaurant/restaurantSagas";

function createAppSaga(...injections) {
  function* appSaga() {
    yield all([
      ...locationSagasFactory(...injections),
      ...restaurantSagasFactory(...injections),
    ]);
  }

  return appSaga;
}

export {
  createAppSaga,
};
