import {
  createStore,
  compose,
  applyMiddleware,
} from "redux";
import createSagaMiddleware from "redux-saga";

import {
  createAppSaga,
} from "./appSagas";
import {
  createAppReducer,
  createInitialReduxStoreState,
} from "./appReducer";
import createApiClient, {defaultApiConfig} from "main/network/apiClient";

function createReduxStore(injections, customInitialState = {},) {
  const sagaMiddleware = createSagaMiddleware();
  const appSaga = createAppSaga({
    mapboxApiClient: createApiClient({
      ...defaultApiConfig,
      baseUrlPrefix: "https://api.mapbox.com",
    }),
  });

  const store = createStore(
    createAppReducer(),
    createInitialReduxStoreState(customInitialState),
    compose(
      applyMiddleware(
        sagaMiddleware,
      ),
    ),
  );

  sagaMiddleware.run(appSaga);

  return store;
}

export {
  createReduxStore,
};
