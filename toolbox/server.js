const path = require("path");

const {
  getCliArgumentValue,
} = require("./cliUtils");
const {
  getEnvironmentVariables,
} = require("./environmentUtils");
const superstatic = require("superstatic").server;

const targetEnv = getCliArgumentValue("--target-env") || "local";
const env = getEnvironmentVariables(targetEnv);

const { DH_HOST: host, DH_PORT: port } = env.raw;
const configFilePath = path.join(__dirname, "..", "config", "server", "superstatic.json");

console.log(`Server config file path is: ${configFilePath}`);
console.log(`Server is starting on host: ${host} and port: ${port}`);

const app = superstatic({
  config: configFilePath,
  port,
  host,
  compression: true,
});

const server = app.listen();

module.exports = server;
