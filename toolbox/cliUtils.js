function getCliArgumentKeyValuePair(argument) {
  const keyValuePairString = process.argv.find(arg => arg === argument) || "";
  const keyValuePairArray = keyValuePairString.split("=");

  let result = {};

  if (keyValuePairArray.length === 2) {
    result = {
      key: keyValuePairArray[0].slice(2),
      value: keyValuePairArray[1],
    };
  }

  return result;
}

function getCliArgumentValue(key) {
  const keyValuePairString = process.argv.find(arg => arg.startsWith(key)) || "";
  const keyValuePairArray = keyValuePairString.split("=");

  return keyValuePairArray[1];
}

function checkForCliArgument(key) {
  return process.argv.includes(arg => arg.startsWith(key));
}

module.exports = {
  getCliArgumentKeyValuePair,
  getCliArgumentValue,
  checkForCliArgument,
};
