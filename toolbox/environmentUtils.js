const path = require("path");
const { getCliArgumentKeyValuePair } = require("./cliUtils");

const ENV_VAR_PREFIX_REGEX = /^DH/i;
const CLI_ARG_PREFIX_REGEX = /^--DH/i;

function getEnvironmentVariables(targetEnv = "local") {
  const baseEnvConfig = require(path.resolve(__dirname, "..", `config/env/${targetEnv}.js`));

  const rawValues = {
    ...baseEnvConfig,
  };

  Object.keys(process.env)
    .filter(key => ENV_VAR_PREFIX_REGEX.test(key))
    .forEach(
      (key) => {
        rawValues[key] = process.env[key];
      },
    );

  process.argv
    .filter(key => CLI_ARG_PREFIX_REGEX.test(key))
    .forEach(
      (arg) => {
        console.log(arg);
        const { key, value } = getCliArgumentKeyValuePair(arg);

        rawValues[key] = value;
      },
    );

  // Stringify all values so we can feed into Webpack DefinePlugin
  const stringifiedProcessEnvValues = {};

  Object.entries(rawValues)
    .forEach(([key, value]) => {
      stringifiedProcessEnvValues[key] = JSON.stringify(value);
    });

  const stringifiedValues = {
    "process.env": stringifiedProcessEnvValues,
  };

  console.log(rawValues);

  return { raw: rawValues, stringified: stringifiedValues };
}

module.exports = {
  getEnvironmentVariables,
};
