# DH Favourites

## Setup

* This project *strictly* requires **Node** `10.15.3` and **yarn** > `1.12.x` for proper development environment. 
(You can use tools like `nvm` to manage multiple **Node** versions easily.)

* Run `yarn install` at the root project folder.

## Development

* You need to create `config/env/local.js` file to start your local development. 
A sample file is present as `config/env/__local.js`, so you can just use it as a template.

* You can customize the base environment configuration by passing CLI arguments named same 
as the value you want to override with a `--` prefix.(ex. `--DH_PORT=4000`) or setting 
environment variables with the same name. Note that CLI arguments takes precedence over 
environment variables. Current target environment types are: `local`.

* Local development server will be available at `localhost:3000` if you use the local config template 
file as it is.

* Run `yarn run start` to create a development bundle and startup the development server.
* Run `yarn run build --target-env=ENV_NAME` to create a release bundle for given target environment (default is `local`).
* Run `yarn run lint:script` to execute static analysis results for `js(x)` files.
* Run `yarn run lint:style` to execute static analysis results for `scss` files.
* Run `yarn run lint:script --fix` to execute static analysis and apply auto-fixes for `js(x)` files.
* Run `yarn run lint:style --fix` to execute static analysis results and apply auto-fixes for `scss` files.
* Run `yarn run test` to execute all test cases.
* Run `yarn run health-check` to execute all static analysis checks and tests cases.

* You can use local `Dockerfile` in order to create a production ready Docker image and run it.

# TODO

* Add more tests for sagas & reducers.
