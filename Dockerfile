FROM node:10.15.3-stretch-slim as health-check

WORKDIR /code/

ADD ./yarn.lock /code/
ADD ./package.json /code/

RUN yarn install --frozen-lockfile && yarn cache clean

ADD ./ /code

RUN yarn run health-check

FROM node:10.15.3-stretch-slim as builder

COPY --from=health-check /code/ /build/
WORKDIR /build/

RUN yarn run build --target-env=prod

RUN mkdir /app
RUN mkdir /app/public
RUN mkdir /app/toolbox
RUN cp -r /build/dist /app
RUN cp -r /build/toolbox /app
RUN rm -rf /build

FROM node:10.15.3-stretch-slim as runtime

COPY --from=builder /app/ /app/
WORKDIR /app/

ARG NODE_ENV=production
ENV NODE_ENV ${NODE_ENV}

WORKDIR /app

ADD ./config/env config/env
ADD ./config/server config/server
RUN npm install superstatic@6.0.4

CMD node ./toolbox/server.js --target-env=prod
