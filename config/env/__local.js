const localConfig = {
  DH_PUBLIC_PATH: "/",
  DH_PORT: 3000,
  DH_HOST: "localhost",
  DH_MAPBOX_ACCESS_TOKEN: "pk.eyJ1IjoibW9ydGFyZ3JpbmQiLCJhIjoiY2p0d3owbTUwMHR5ejQ0cGxkZzRiNmo3biJ9.u2KirBb88ITqf9Xcbxw0yw",
};

module.exports = localConfig;
