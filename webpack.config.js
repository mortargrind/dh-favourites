const path = require("path");

const webpack = require("webpack");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

const {getCliArgumentValue} = require("./toolbox/cliUtils");
const {getEnvironmentVariables} = require("./toolbox/environmentUtils");
const postCssConfig = require("./postcss.config.js");

const isDebug = !process.argv.includes("--release");
const targetEnv = getCliArgumentValue("--target-env") || "local";
const outPath = path.join(__dirname, "dist");
const srcPath = path.join(__dirname, "src");

const targetEnvVariables = getEnvironmentVariables(targetEnv);
const DH_VERSION = Date.now();

const babelLoaderConfig = {
  loader: "babel-loader",
  options: {
    cacheDirectory: isDebug,
  },
};

const config = {
  target: "web",

  mode: isDebug ? "development" : "production",

  entry: {
    app: [
      path.join(srcPath, "/index.jsx"),
    ],
  },

  output: {
    path: outPath,
    filename: isDebug ? "[name].js" : "[name]-[hash].js",
    chunkFilename: isDebug ? "[name].js" : "[name]-[chunkhash].js",
    publicPath: targetEnvVariables.raw.DH_PUBLIC_PATH,
  },

  module: {
    // Make missing exports an error instead of warning
    strictExportPresence: true,

    rules: [
      {parser: {amd: false}},
      {
        test: /\.(js|jsx|mjs)$/,
        include: [
          srcPath,
        ],
        use: [
          babelLoaderConfig,
        ]
      },

      {
        test: /\.css$/,
        use: [
          isDebug ? {
            loader: "style-loader",
            options: {
              insertAt: "top",
              singleton: true,
              sourceMap: isDebug,
            },
          } : MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              modules: false,
              importLoaders: 1,
              sourceMap: isDebug,
            },
          },
          {
            loader: "postcss-loader",
            options: postCssConfig,
          },
        ],
      },

      {
        test: /\.scss$/,
        use: [
          isDebug ? {
            loader: "style-loader",
            options: {
              insertAt: "top",
              singleton: true,
              sourceMap: isDebug,
            },
          } : MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              modules: true,
              importLoaders: 1,
              sourceMap: isDebug,
              localIdentName: "[path]_[name]_[local]",
            },
          },
          {
            loader: "postcss-loader",
            options: postCssConfig,
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: isDebug,
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        loader: "svg-url-loader",
      },
      {
        test: /\.(png|jpg|ttf|otf)$/,
        use: [
          {
            loader: "file-loader",
          },
        ],
      },
    ],
  },

  resolve: {
    extensions: [".jsx", ".mjs", ".js", ".json"],

    modules: [
      path.resolve(__dirname, "src"),
      "node_modules",
    ],

    alias: {
      asset: path.resolve(__dirname, "asset"),
    },
  },

  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        sourceMap: true,
        parallel: true,
      }),
      new OptimizeCSSAssetsPlugin({}),
    ],
    noEmitOnErrors: true,
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendor",
          chunks: "initial",
        },
      },
    },
  },

  stats: "errors-only",

  // Choose a developer tool to enhance debugging
  devtool: isDebug ? "cheap-module-inline-source-map" : (targetEnv !== "prod" && "source-map"),

  plugins: [
    ...(isDebug ? [] : [
      new CleanWebpackPlugin(
        {
          verbose: true,
        },
      ),
    ]),


    new webpack.DefinePlugin(
      {
        ...targetEnvVariables.stringified,
        "process.env.DH_VERSION": JSON.stringify(DH_VERSION),
      }
    ),

    new HtmlWebpackPlugin({
      template: path.join(
        srcPath,
        "index.html",
      ),
      DH_TARGET_ENV: targetEnv,
      filename: "index.html",
      inject: "body",
    }),

    ...(isDebug ? [] : [
      new MiniCssExtractPlugin({
        filename: "[name]-[contenthash].css",
        chunkFilename: "[id]-[contenthash].css",
      }),
    ]),
  ],

  devServer: {
    inline: true,
    contentBase: outPath,
    compress: true,
    port: targetEnvVariables.raw.DH_PORT,
    disableHostCheck: true,
    historyApiFallback: true,
  }
};


module.exports = config;
